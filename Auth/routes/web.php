<?php

use App\Http\Controllers\Role\AdminController;
use App\Http\Controllers\Role\CookController;
use App\Http\Controllers\Role\CourierController;
use App\Http\Controllers\Role\CustomerController;
use App\Http\Controllers\Role\ManagerController;
use App\Http\Controllers\Role\ModeratorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', [AdminController::class, 'index']);
Route::get('/moderator', [ModeratorController::class, 'index']);
Route::get('/customer', [CustomerController::class, 'index']);
Route::get('/cook', [CookController::class, 'index']);
Route::get('/manager', [ManagerController::class, 'index']);
Route::get('/courier', [CourierController::class, 'index']);
