<?php

use App\Http\Controllers\Api\AddressController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\RoleUserController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::middleware('auth:api')->group(function () {
//    Route::post('/account/logout', [AuthController::class, 'logout']);

    Route::controller(ProfileController::class)->group(function () {
        Route::get('/account/profile', 'show');
        Route::put('/account/profile', 'update');
    });

    Route::controller(UserController::class)->group(function () {
       Route::get('/users', 'show_all');
       Route::get('/users/{id}', 'show');
       Route::put('/users/{id}', 'update');
       Route::delete('/users/{id}', 'delete');
    });

    Route::controller(AddressController::class)->group(function () {
       Route::get('/addresses', 'show_all');
       Route::post('/addresses', 'create');
       Route::get('/addresses/{id}', 'show');
       Route::put('/addresses/{id}', 'update');
       Route::delete('/addresses/{id}', 'delete');
    });

    Route::controller(RoleController::class)->group(function () {
       Route::get('/roles', 'show_all');
       Route::get('/roles/{id}', 'show'); //get role by role's id
    });

    Route::controller(RoleUserController::class)->group(function () {
        Route::patch('/roles/{user_id}', 'update'); // create new roles for user
        Route::delete('/roles/{user_id}', 'delete'); // delete existing roles for user
    });
//});

Route::controller(AuthController::class)->group(function () {
   Route::post('/account/register', 'register');
   Route::post('/account/login', 'login');
   Route::post('/account/logout', 'logout');
});
