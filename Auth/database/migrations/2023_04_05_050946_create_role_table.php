<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('role')->unique();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE roles ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('user_roles', function (Blueprint $table) {
           $table->uuid('id')->unique();
           $table->foreignUuid('user_id')->constrained('users')->cascadeOnDelete();
           $table->foreignUuid('role_id')->constrained('roles')->cascadeOnDelete();
        });
        DB::statement('ALTER TABLE user_roles ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
        Schema::dropIfExists('user_roles');
    }
};
