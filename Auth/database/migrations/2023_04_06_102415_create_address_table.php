<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('full_address');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE addresses ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('users_addresses', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->foreignUuid('user_id')->constrained('users')->cascadeOnDelete();
            $table->foreignUuid('address_id')->constrained('addresses')->cascadeOnDelete();
        });
        DB::statement('ALTER TABLE users_addresses ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('users_addresses');
    }
};
