<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                ['id' => 'a44901be-015b-47be-84cb-0ede775a6526', 'full_name' => 'Емельянова Милана Владиславовна', 'birth_date' => '2002-10-31', 'gender' => 'female', 'phone' => '7(908)439-05-09', 'email' => 'vileuppeizaummoi-7679@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B70166'],
                ['id' => 'f1cf4734-06d0-4be8-97da-d898fe522b47', 'full_name' => 'Нефедов Артём Никитич', 'birth_date' => '2000-07-20', 'gender' => 'male', 'phone' => '7(908)689-93-83', 'email' => 'trafrauppeiffetrei-1197@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'af1596f8-c691-4f61-8876-41b6fdb7a07a', 'full_name' => 'Тихонов Ярослав Владимирович', 'birth_date' => '1998-01-13', 'gender' => 'male', 'phone' => '7(908)433-98-59', 'email' => 'bullafrebrommi-7377@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '2c8efd7c-96b6-43f7-ae38-6cb2c3d87a38', 'full_name' => 'Васильева София Лукинична', 'birth_date' => '1996-08-25', 'gender' => 'female', 'phone' => '7(908)103-74-66', 'email' => 'fraweppocatte-6108@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'ac411421-5440-4e76-b9e0-2c831a8ac22a', 'full_name' => 'Белов Константин Робертович', 'birth_date' => '2003-10-26', 'gender' => 'male', 'phone' => '7(908)969-56-93', 'email' => 'pippouheibrauwa-4650@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'a4b8d0d2-b24a-4e43-b0d3-6ccd05032bd4', 'full_name' => 'Анисимова Полина Александровна', 'birth_date' => '1993-06-25', 'gender' => 'female', 'phone' => '7(908)823-11-65', 'email' => 'brotruttossoippa-3535@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '6b73204d-8b91-459b-983d-61fa849998a5', 'full_name' => 'Кузнецов Елисей Тимофеевич', 'birth_date' => '1991-02-16', 'gender' => 'male', 'phone' => '7(908)085-84-54', 'email' => 'yeigravoibreje-3030@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '404ccf25-a365-438c-b7a0-ca5e2d57e43b', 'full_name' => 'Трошина Полина Данииловна', 'birth_date' => '1992-08-17', 'gender' => 'female', 'phone' => '7(908)583-04-90', 'email' => 'loutratawausa-8978@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'f4d2d3b8-fd38-4c50-a722-67bb85e5a68a', 'full_name' => 'Царев Михаил Семёнович', 'birth_date' => '2001-09-16', 'gender' => 'male', 'phone' => '7(908)699-29-36', 'email' => 'renimesotro-6025@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '48eed20c-4d6b-4bda-b899-d98c9af010c4', 'full_name' => 'Сальникова Виктория Григорьевна', 'birth_date' => '1980-02-14', 'gender' => 'male', 'phone' => '7(908)609-86-54', 'email' => 'peizappofuje-1209@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '794ca20a-9471-46d7-ad23-21765cbd4eb8', 'full_name' => 'Максимов Дмитрий Лукич', 'birth_date' => '1991-10-19', 'gender' => 'male', 'phone' => '7(908)665-64-69', 'email' => 'quottuprajezou-8566@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '4a4a95f2-b312-4c70-9fb8-21dab0defbbd', 'full_name' => 'Дубровина Виктория Демьяновна', 'birth_date' => '1985-12-11', 'gender' => 'female', 'phone' => '7(908)332-90-14', 'email' => 'geboicrafreutro-2635@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '3937cc8b-156f-4e3d-94cd-e1a6fbd12747', 'full_name' => 'Игнатьева Анна Артёмовна', 'birth_date' => '1984-08-08', 'gender' => 'female', 'phone' => '7(908)831-26-01', 'email' => 'coucezautteitto-8348@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'b951a693-367f-46c6-bb89-cb219769e89d', 'full_name' => 'Позднякова Агата Адамовна', 'birth_date' => '2001-08-28', 'gender' => 'female', 'phone' => '7(908)313-58-69', 'email' => 'neuffugivausi-3701@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '37adca82-697b-4a88-af05-1cb8f68b8a5c', 'full_name' => 'Гаврилов Максим Васильевич', 'birth_date' => '2000-11-11', 'gender' => 'male', 'phone' => '7(908)341-71-04', 'email' => 'tonazibrafrou-8791@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'a52f108b-122e-400a-9531-24d7d625bc4f', 'full_name' => 'Волкова Пелагея Даниэльевна', 'birth_date' => '2003-06-01', 'gender' => 'female', 'phone' => '7(908)373-48-80', 'email' => 'fruroippalobou-5268@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '3e72f9e6-15fe-455d-b4bb-12408fea1d63', 'full_name' => 'Зайцева Аделина Тимуровна', 'birth_date' => '1988-12-07', 'gender' => 'female', 'phone' => '7(908)119-72-16', 'email' => 'brennourasuneu-8014@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '586f87a5-9ff2-4025-af00-9d711f1e1818', 'full_name' => 'Гусев Святослав Романович', 'birth_date' => '1980-11-06', 'gender' => 'male', 'phone' => '7(908)201-16-16', 'email' => 'gottofikipoi-5900@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '9b145739-8131-43bd-a527-ddd546eee05e', 'full_name' => 'Михеев Ярослав Евгеньевич', 'birth_date' => '2001-03-21', 'gender' => 'male', 'phone' => '7(908)587-68-21', 'email' => 'seigayijelle-1052@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '55c05bbf-ffbc-4d73-bf88-b212d058283a', 'full_name' => 'Никитин Сергей Степанович', 'birth_date' => '1981-06-27', 'gender' => 'male', 'phone' => '7(908)047-92-76', 'email' => 'keffeittatresa-9261@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '4d570d39-311b-4ee5-a3ff-652eae618b3c', 'full_name' => 'Нестеров Михаил Георгиевич', 'birth_date' => '1991-06-18', 'gender' => 'male', 'phone' => '7(908)411-74-37', 'email' => 'tannolawevoi-5728@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'd74f8502-a167-4454-abe2-ad065d102a4c', 'full_name' => 'Нестеров Михаил Георгиевич', 'birth_date' => '1996-08-12', 'gender' => 'male', 'phone' => '7(908)246-78-09', 'email' => 'prummouboboupeu-1770@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '6e7bfff7-6c5f-4017-bfc3-a1aa8f874c5a', 'full_name' => 'Кудряшов Фёдор Владимирович', 'birth_date' => '1980-07-12', 'gender' => 'male', 'phone' => '7(908)915-37-43', 'email' => 'gegrahujonna-5840@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '50b287ca-b77a-49bb-b5f7-424e898ebf09', 'full_name' => 'Сорокин Вячеслав Григорьевич', 'birth_date' => '1985-10-10', 'gender' => 'male', 'phone' => '7(908)400-73-94', 'email' => 'xufreinussaco-1426@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '117526aa-3077-4a32-a331-a5e4a46f1433', 'full_name' => 'Большакова Анна Елисеевна', 'birth_date' => '1991-11-09', 'gender' => 'female', 'phone' => '7(908)155-58-21', 'email' => 'frapessiyeixi-2831@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '739db128-aa21-4c68-8e7d-d9b8cf915441', 'full_name' => 'Михайлов Никита Тимофеевич', 'birth_date' => '1991-12-02', 'gender' => 'male', 'phone' => '7(908)788-68-83', 'email' => 'crecomecruquo-8907@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '728086c1-3082-420c-8625-043d171bfc3d', 'full_name' => 'Морозова Елизавета Ивановна', 'birth_date' => '1991-03-31', 'gender' => 'female', 'phone' => '7(908)980-73-43', 'email' => 'yeuyeriwuja-5125@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '36fc2474-873e-4b11-88c5-57652f42f3b5', 'full_name' => 'Смирнова Светлана Мироновна', 'birth_date' => '1989-03-20', 'gender' => 'female', 'phone' => '7(908)435-83-96', 'email' => 'heffoigrafreuwo-4138@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '1de4ac5f-c90e-4607-8ff0-f7a56c780707', 'full_name' => 'Мещерякова Яна Климовна', 'birth_date' => '1996-05-09', 'gender' => 'female', 'phone' => '7(908)510-44-25', 'email' => 'xecraquissoippa-7221@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '50b83b2a-d476-4486-9a31-bd5c19dc6220', 'full_name' => 'Баранов Савелий Глебович', 'birth_date' => '001-03-24', 'gender' => 'male', 'phone' => '7(908)038-81-16', 'email' => 'hoibrupriwellu-7836@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'aa62560e-791c-49b1-89ed-7d84e0e12037', 'full_name' => 'Петров Максим Данилович', 'birth_date' => '1985-10-12', 'gender' => 'male', 'phone' => '7(908)881-28-28', 'email' => 'brouzereudaummi-2202@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '6c1a5df7-1047-4efd-b2e3-569a6c2c0974', 'full_name' => 'Воробьева София Кирилловна', 'birth_date' => '1985-12-25', 'gender' => 'female', 'phone' => '7(908)253-35-33', 'email' => 'gummoufihoumi-6770@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '5c5f6e12-9244-41bd-b20d-074eb3211119', 'full_name' => 'Козлова Анастасия Владимировна', 'birth_date' => '1987-01-04', 'gender' => 'female', 'phone' => '7(908)713-34-36', 'email' => 'bassiboddacra-6179@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '69ef61f0-3f56-4def-a0bc-fa49f37f0636', 'full_name' => 'Никитина Полина Львовна', 'birth_date' => '1998-08-19', 'gender' => 'female', 'phone' => '7(908)601-34-48', 'email' => 'boddeitturauzou-8235@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'b2f3514e-6255-4082-98fe-3d01f2638e67', 'full_name' => 'Горбунова Ева Кирилловна', 'birth_date' => '2000-05-25', 'gender' => 'female', 'phone' => '7(908)205-12-33', 'email' => 'gaulavoxoca-3015@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'b3e83a69-49f4-46a1-8848-2db7b25a7b23', 'full_name' => 'Малышева Нина Даниэльевна', 'birth_date' => '1983-05-23', 'gender' => 'female', 'phone' => '7(908)879-40-20', 'email' => 'pauwowiteha-1065@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '8b070603-52de-40ad-b711-675622aa1b8f', 'full_name' => 'Иванов Василий Кириллович', 'birth_date' => '1986-10-18', 'gender' => 'male', 'phone' => '7(908)442-87-92', 'email' => 'vaucroucrouttutto-7302@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'ca6635de-7755-4763-9db8-0247da91d49a', 'full_name' => 'Фетисова Арина Сергеевна', 'birth_date' => '1995-02-13', 'gender' => 'female', 'phone' => '7(908)937-47-66', 'email' => 'xaddacrolave-6337@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '21bb5d5f-7320-4fc3-b4f6-dbfebe99b233', 'full_name' => 'Трофимов Даниил Иванович', 'birth_date' => '2003-02-22', 'gender' => 'male', 'phone' => '7(908)622-95-55', 'email' => 'freitrafauqueke-8711@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => '14790c40-c6ce-48f4-b4ac-eda1bf2eecc7', 'full_name' => 'Исаев Артём Романович', 'birth_date' => '2001-01-05', 'gender' => 'male', 'phone' => '7(908)247-37-81', 'email' => 'tecriweibrase-2795@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
                ['id' => 'ab2e9685-7f38-4633-8369-55d76895a52c', 'full_name' => 'Иванов Никита Андреевич', 'birth_date' => '1995-12-15', 'gender' => 'male', 'phone' => '7(908)889-95-43', 'email' => 'vuraulocullo-2436@yopmail.com', 'password' => '698D51A19D8A121CE581499D7B701668'],
            ]
        );
    }
}
