<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                ['id' => '442ab61d-ec90-480a-9c40-a48247c2a164', 'role' => 'customer'],
                ['id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8', 'role' => 'courier'],
                ['id' => '907468ff-1327-406c-9027-8353a5fdf8ad', 'role' => 'manager'],
                ['id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a', 'role' => 'cook'],
                ['id' => 'a490a596-e15e-4244-a438-212a1b406f73', 'role' => 'admin'],
                ['id' => '921a3a5f-45df-494c-b3a7-2899d8305da0', 'role' => 'moderator'],
            ]
        );
    }
}
