<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id' => 'b11961ff-888f-4ee1-a02e-f76aa360f940', 'user_id' => 'a44901be-015b-47be-84cb-0ede775a6526', 'address_id' => 'b30707ef-3343-4293-afd3-9c60e3323667'],
            ['id' => 'a60f0d59-1a98-4173-8fc0-4de5bd40b196', 'user_id' => 'f1cf4734-06d0-4be8-97da-d898fe522b47', 'address_id' => '80b2217a-b9ca-483a-a165-09cafece5cb1'],
            ['id' => '30f0c8ec-6bdd-48df-a58e-52979d047404', 'user_id' => 'af1596f8-c691-4f61-8876-41b6fdb7a07a', 'address_id' => '57b6dc99-d4d4-463d-817c-f7a7dcbd46c9'],
            ['id' => '2eb97814-0cad-44a9-9b47-62cf44ec868a', 'user_id' => '2c8efd7c-96b6-43f7-ae38-6cb2c3d87a38', 'address_id' => '6c18e575-8f51-420a-9712-a7e82d780de7'],
            ['id' => 'c6eb4a76-a21e-4153-b271-be38c73de6fe', 'user_id' => 'ac411421-5440-4e76-b9e0-2c831a8ac22a', 'address_id' => '45b32f48-7204-4428-9c1b-13cbae4e69e4'],
            ['id' => '0f27cb99-0e20-414c-b150-f1a91994896e', 'user_id' => 'a4b8d0d2-b24a-4e43-b0d3-6ccd05032bd4', 'address_id' => '9d1fd8fb-33fe-4eab-ad5e-a76132896446'],
            ['id' => '937e242b-78bf-49d0-a161-8b587b920542', 'user_id' => '6b73204d-8b91-459b-983d-61fa849998a5', 'address_id' => 'c7a8e059-87d5-408c-aff8-5f67ea6cc4e2'],
            ['id' => '65753b6a-a297-4daf-b708-dcbff7cef10b', 'user_id' => '404ccf25-a365-438c-b7a0-ca5e2d57e43b', 'address_id' => '5ed05298-ef0c-424b-9f75-0375a8994990'],
            ['id' => '1db426fe-4f97-4ae5-a84e-276702dbfc7c', 'user_id' => 'f4d2d3b8-fd38-4c50-a722-67bb85e5a68a', 'address_id' => 'd13ea740-b564-4eb9-a1d7-2a6bd877454a'],
            ['id' => '7701c8a2-25e5-4a40-accb-1202b5befa43', 'user_id' => '48eed20c-4d6b-4bda-b899-d98c9af010c4', 'address_id' => 'e97e6530-6a9f-4039-9132-42de59ee03e2'],
            ['id' => 'cc739a21-7136-4549-9d8e-125245d54a27', 'user_id' => '794ca20a-9471-46d7-ad23-21765cbd4eb8', 'address_id' => '007877a8-fb44-4cea-865d-b5f9fba80988'],
            ['id' => 'c5a59a75-8ed9-499a-b586-495f44b0ff9c', 'user_id' => '4a4a95f2-b312-4c70-9fb8-21dab0defbbd', 'address_id' => 'efe415a2-cbfa-43bd-8878-1aec76f9266b'],
            ['id' => 'd478759a-ceec-4d22-b5dc-0156c97e707b', 'user_id' => '3937cc8b-156f-4e3d-94cd-e1a6fbd12747', 'address_id' => '5554285e-0989-4ff0-92a6-c2b548c81a15'],
            ['id' => '1679120c-76f2-4956-9f01-93894502627c', 'user_id' => 'b951a693-367f-46c6-bb89-cb219769e89d', 'address_id' => '78f8505b-5300-4101-8be3-7de0cf6e3811'],
            ['id' => '83b903f6-ee39-466e-a786-8e1942a70c62', 'user_id' => '37adca82-697b-4a88-af05-1cb8f68b8a5c', 'address_id' => 'a1d522cb-b78f-44ef-ae46-ec419ecc954a'],
            ['id' => '58aef17a-35ce-4772-962b-2de0bbad691e', 'user_id' => 'a52f108b-122e-400a-9531-24d7d625bc4f', 'address_id' => 'e2d8ba2d-ef36-4756-83da-e6039b03b07f'],
            ['id' => '690eb783-d9cd-4407-b7a8-17950dbb98f9', 'user_id' => '3e72f9e6-15fe-455d-b4bb-12408fea1d63', 'address_id' => '8a204797-2a7e-4f39-842f-7d7ba71e79c0'],
            ['id' => '788f7378-fbb6-499f-ba11-9f6d300790a0', 'user_id' => '586f87a5-9ff2-4025-af00-9d711f1e1818', 'address_id' => 'f007c775-f739-4fea-b7a4-99a5e85130f1'],
            ['id' => 'f39991ab-d58f-420d-9742-8ec82324a767', 'user_id' => '9b145739-8131-43bd-a527-ddd546eee05e', 'address_id' => 'b2af3303-9051-49a0-b871-2d551cf61197'],
            ['id' => '289f8df8-5919-42ae-b504-d6ed41a6ede7', 'user_id' => '55c05bbf-ffbc-4d73-bf88-b212d058283a', 'address_id' => 'd46875da-cf66-48ad-855b-70f77bd6d3ca'],
            ['id' => 'cc1416fe-ffe0-47a1-b2f8-6899f0a25538', 'user_id' => '4d570d39-311b-4ee5-a3ff-652eae618b3c', 'address_id' => '113070a2-78ea-4d55-99bc-b7affe68fa0d'],
            ['id' => 'd0883a51-050f-43e6-82fa-e64165136807', 'user_id' => 'd74f8502-a167-4454-abe2-ad065d102a4c', 'address_id' => '3c8a8859-1bd4-4059-b85b-fd408bb70b8c'],
            ['id' => 'fc6401a6-bb78-437a-9d04-6af4ac7334e3', 'user_id' => '6e7bfff7-6c5f-4017-bfc3-a1aa8f874c5a', 'address_id' => 'f9a0b88c-f4fd-4312-8ff8-5c7119f56169'],
            ['id' => '3190d863-8f9e-4a9d-8f14-c62601913356', 'user_id' => '50b287ca-b77a-49bb-b5f7-424e898ebf09', 'address_id' => '2fa19fe5-6ad7-496a-869b-3d09e0ef7996'],
            ['id' => 'a34210b6-c45c-46bb-af22-9e106bfa6ff5', 'user_id' => '117526aa-3077-4a32-a331-a5e4a46f1433', 'address_id' => '5238bb94-39f1-474f-a8bd-efe15cc37a5b'],
            ['id' => 'bb684abf-155d-4fa6-84f7-b024169024f9', 'user_id' => '739db128-aa21-4c68-8e7d-d9b8cf915441', 'address_id' => 'f860947e-9fc1-47ac-a795-00a2469fe02b'],
            ['id' => '68c5ac21-92d7-4213-9aa8-261b4ebf8930', 'user_id' => '728086c1-3082-420c-8625-043d171bfc3d', 'address_id' => 'aeb5d416-0fd4-4d3a-8d4b-da05704f717e'],
            ['id' => '808ffff1-7cef-4846-a803-4b7e730a358d', 'user_id' => '36fc2474-873e-4b11-88c5-57652f42f3b5', 'address_id' => '7266448f-beb9-4394-b57d-f075c679f156'],
            ['id' => '8f5d0ccc-8199-4a50-8758-1ded0475e2e7', 'user_id' => '1de4ac5f-c90e-4607-8ff0-f7a56c780707', 'address_id' => '04a0a908-477b-489c-9880-eaddbc3f6ae2'],
            ['id' => '375c3e3b-01af-4f4a-8a50-eb397f4d1bbb', 'user_id' => '50b83b2a-d476-4486-9a31-bd5c19dc6220', 'address_id' => '1ce3dd1c-116c-4cfc-9f39-6138fa1f1c5b'],
            ['id' => 'b4bbacfd-5388-4c46-bef5-d1242aafe4dd', 'user_id' => 'aa62560e-791c-49b1-89ed-7d84e0e12037', 'address_id' => '647f8a86-87a5-4a07-8dbc-d37ec0534524'],
            ['id' => 'a9b7b593-871a-4691-a5c4-6365002a85fe', 'user_id' => '6c1a5df7-1047-4efd-b2e3-569a6c2c0974', 'address_id' => '436e25f2-aa61-41f3-969e-69ea0660e84b'],
            ['id' => '83f37682-6b40-461d-9815-80bdf6a07e82', 'user_id' => '5c5f6e12-9244-41bd-b20d-074eb3211119', 'address_id' => '53a0391a-d37b-423d-b425-ca774a23aeeb'],
            ['id' => 'e222c213-66be-4bea-833f-d8f75d6be102', 'user_id' => '69ef61f0-3f56-4def-a0bc-fa49f37f0636', 'address_id' => '0b5ba09f-4404-4243-a03e-f21b8552bd14'],
            ['id' => '511063f9-adf0-4af0-b341-8f6bcaf154af', 'user_id' => 'b2f3514e-6255-4082-98fe-3d01f2638e67', 'address_id' => '68065abf-aac2-4527-a884-a9909a3f012f'],
            ['id' => '5ed1aaa7-7992-46a1-8795-f1a854c362af', 'user_id' => 'b3e83a69-49f4-46a1-8848-2db7b25a7b23', 'address_id' => '243769a2-a6b9-4195-904e-da2580ded50c'],
            ['id' => 'f8d1b04c-f382-4f5d-a3af-724994bdd49a', 'user_id' => '8b070603-52de-40ad-b711-675622aa1b8f', 'address_id' => '33ffb1d0-d72a-4072-9efe-2dc700b68b2a'],
            ['id' => '9bf7b37f-c65e-4c52-8109-516fea03d31f', 'user_id' => 'ca6635de-7755-4763-9db8-0247da91d49a', 'address_id' => 'e640584e-71c5-4608-8f13-777b5709930e'],
            ['id' => 'b6e3c47c-3f5b-456f-b6ac-7054761dc129', 'user_id' => '21bb5d5f-7320-4fc3-b4f6-dbfebe99b233', 'address_id' => '7fd4ad4a-5742-4bd1-9283-2a7aa6845dc2'],
            ['id' => '6ac5f472-df23-47a0-92c2-2d00f8ddaba2', 'user_id' => '14790c40-c6ce-48f4-b4ac-eda1bf2eecc7', 'address_id' => '40e98ea8-8ae1-412f-b3c4-9f4d771e44d5'],
            ['id' => 'fc5ebc76-0248-4b8f-aad2-051497f60ccf', 'user_id' => 'ab2e9685-7f38-4633-8369-55d76895a52c', 'address_id' => 'bca80118-cd5b-4886-87a3-63df62439d85'],
            ['id' => 'c6419eee-17b1-41da-9589-8f609b6720e9', 'user_id' => '6e7bfff7-6c5f-4017-bfc3-a1aa8f874c5a', 'address_id' => '58c40188-323c-4cae-b287-e477bfc52bdd'],
            ['id' => '14252b88-1b49-4313-8999-2762eabd16fc', 'user_id' => '586f87a5-9ff2-4025-af00-9d711f1e1818', 'address_id' => '0222cec0-b81d-42cc-8606-32348b501d14'],
            ['id' => 'd24228b5-1f4a-43f9-9193-4c12703dde8b', 'user_id' => '794ca20a-9471-46d7-ad23-21765cbd4eb8', 'address_id' => '03b4f90b-2cfe-4ad6-99d0-3fdc4c7f4b6b'],
            ['id' => 'b64e55e8-203f-4b98-a27f-1a5192daaab9', 'user_id' => '728086c1-3082-420c-8625-043d171bfc3d', 'address_id' => '6809e330-ad74-4d65-bc14-32a7a4571059'],
            ['id' => '46f66c72-26a7-4029-a555-1159970ea3f6', 'user_id' => '2c8efd7c-96b6-43f7-ae38-6cb2c3d87a38', 'address_id' => '5280b5bb-b871-43b9-9cbd-c828b88ba335'],
            ['id' => '16e21b01-9622-4a0b-8eee-99580532a730', 'user_id' => 'f1cf4734-06d0-4be8-97da-d898fe522b47', 'address_id' => '35fc6e4f-c134-4cb2-8ece-ed7779e83832'],
            ['id' => 'f6ac3e09-227a-4902-9fba-fb6e7ad66401', 'user_id' => '69ef61f0-3f56-4def-a0bc-fa49f37f0636', 'address_id' => '3d86a189-df94-4ed3-ba15-b77a6acc3864'],
            ['id' => '743ffa91-bc95-482f-aace-37d835b7d50e', 'user_id' => '586f87a5-9ff2-4025-af00-9d711f1e1818', 'address_id' => 'bdbf8818-113f-4e2d-a82d-909ac087a6e6'],
            ['id' => '64b7b4a5-716b-4a89-8846-9c1b77b7498a', 'user_id' => '1de4ac5f-c90e-4607-8ff0-f7a56c780707', 'address_id' => '72d1f60e-63e5-4c4c-96b2-e06b50497c8e'],
            ['id' => 'bf3613bd-399c-49ff-b81d-f33a24531b30', 'user_id' => '9b145739-8131-43bd-a527-ddd546eee05e', 'address_id' => 'b30707ef-3343-4293-afd3-9c60e3323667'],
        ]);
    }
}
