<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('addresses')->insert([
            ['id' => 'bca80118-cd5b-4886-87a3-63df62439d85', 'full_address' => 'Набережная ул., д. 5 кв.75'],
            ['id' => '58c40188-323c-4cae-b287-e477bfc52bdd', 'full_address' => 'Трудовая ул., д. 5 кв.11'],
            ['id' => '0222cec0-b81d-42cc-8606-32348b501d14', 'full_address' => 'Западная ул., д. 3 кв.141'],
            ['id' => '03b4f90b-2cfe-4ad6-99d0-3fdc4c7f4b6b', 'full_address' => 'Центральная ул., д. 22 кв.32'],
            ['id' => '6809e330-ad74-4d65-bc14-32a7a4571059', 'full_address' => 'Полевая ул., д. 11 кв.33'],
            ['id' => '5280b5bb-b871-43b9-9cbd-c828b88ba335', 'full_address' => 'Октябрьская ул., д. 16 кв.99'],
            ['id' => '35fc6e4f-c134-4cb2-8ece-ed7779e83832', 'full_address' => 'Радужная ул., д. 18 кв.23'],
            ['id' => '3d86a189-df94-4ed3-ba15-b77a6acc3864', 'full_address' => 'Центральный пер., д. 7 кв.75'],
            ['id' => 'bdbf8818-113f-4e2d-a82d-909ac087a6e6', 'full_address' => 'Чкалова ул., д. 5 кв.74'],
            ['id' => '72d1f60e-63e5-4c4c-96b2-e06b50497c8e', 'full_address' => 'Молодежный пер., д. 5 кв.134'],
            ['id' => 'b30707ef-3343-4293-afd3-9c60e3323667', 'full_address' => 'Красноармейская ул., д. 11 кв.18'],
            ['id' => '80b2217a-b9ca-483a-a165-09cafece5cb1', 'full_address' => 'ЯнкиКупалы ул., д. 23 кв.15'],
            ['id' => '57b6dc99-d4d4-463d-817c-f7a7dcbd46c9', 'full_address' => 'Строителей ул., д. 2 кв.69'],
            ['id' => '6c18e575-8f51-420a-9712-a7e82d780de7', 'full_address' => 'Тихая ул., д. 11 кв.200'],
            ['id' => '45b32f48-7204-4428-9c1b-13cbae4e69e4', 'full_address' => 'Новоселов ул., д. 6 кв.43'],
            ['id' => '9d1fd8fb-33fe-4eab-ad5e-a76132896446', 'full_address' => 'Школьная ул., д. 25 кв.16'],
            ['id' => 'c7a8e059-87d5-408c-aff8-5f67ea6cc4e2', 'full_address' => 'Первомайский пер., д. 5 кв.103'],
            ['id' => '5ed05298-ef0c-424b-9f75-0375a8994990', 'full_address' => 'Первомайский пер., д. 4 кв.124'],
            ['id' => 'd13ea740-b564-4eb9-a1d7-2a6bd877454a', 'full_address' => 'Центральная ул., д. 23 кв.186'],
            ['id' => 'e97e6530-6a9f-4039-9132-42de59ee03e2', 'full_address' => 'Зеленая ул., д. 4 кв.128'],
            ['id' => '007877a8-fb44-4cea-865d-b5f9fba80988', 'full_address' => 'Мичурина ул., д. 16 кв.178'],
            ['id' => 'efe415a2-cbfa-43bd-8878-1aec76f9266b', 'full_address' => 'Восточная ул., д. 9 кв.162'],
            ['id' => '5554285e-0989-4ff0-92a6-c2b548c81a15', 'full_address' => 'Севернаяул., д. 25 кв.43'],
            ['id' => '78f8505b-5300-4101-8be3-7de0cf6e3811', 'full_address' => 'Солнечный пер., д. 23 кв.76'],
            ['id' => 'a1d522cb-b78f-44ef-ae46-ec419ecc954a', 'full_address' => 'Майская ул., д. 3 кв.175'],
            ['id' => 'e2d8ba2d-ef36-4756-83da-e6039b03b07f', 'full_address' => 'Железнодорожная ул., д. 5 кв.116'],
            ['id' => '8a204797-2a7e-4f39-842f-7d7ba71e79c0', 'full_address' => 'Заслонова ул., д. 19 кв.42'],
            ['id' => 'f007c775-f739-4fea-b7a4-99a5e85130f1', 'full_address' => 'Рабочая ул., д. 15 кв.21'],
            ['id' => 'b2af3303-9051-49a0-b871-2d551cf61197', 'full_address' => 'Майская ул., д. 1 кв.79'],
            ['id' => 'd46875da-cf66-48ad-855b-70f77bd6d3ca', 'full_address' => 'Речной пер., д. 6 кв.81'],
            ['id' => 'e5fabea3-03eb-48b9-b881-1e25a42a4ce4', 'full_address' => 'Спортивная ул., д. 11 кв.158'],
            ['id' => '113070a2-78ea-4d55-99bc-b7affe68fa0d', 'full_address' => 'Чапаева ул., д. 4 кв.181'],
            ['id' => '3c8a8859-1bd4-4059-b85b-fd408bb70b8c', 'full_address' => 'Солнечная ул., д. 3 кв.141'],
            ['id' => 'f9a0b88c-f4fd-4312-8ff8-5c7119f56169', 'full_address' => 'Победы ул., д. 11 кв.194'],
            ['id' => '2fa19fe5-6ad7-496a-869b-3d09e0ef7996', 'full_address' => 'Тихая ул., д. 21 кв.85'],
            ['id' => '5238bb94-39f1-474f-a8bd-efe15cc37a5b', 'full_address' => 'Школьный пер., д. 11 кв.80'],
            ['id' => 'f860947e-9fc1-47ac-a795-00a2469fe02b', 'full_address' => 'Набережная ул., д. 3 кв.88'],
            ['id' => 'aeb5d416-0fd4-4d3a-8d4b-da05704f717e', 'full_address' => 'Садовая ул., д. 24 кв.18'],
            ['id' => '7266448f-beb9-4394-b57d-f075c679f156', 'full_address' => 'Кирова ул., д. 24 кв.25'],
            ['id' => '04a0a908-477b-489c-9880-eaddbc3f6ae2', 'full_address' => 'Дорожная ул., д. 1 кв.90'],
            ['id' => '1ce3dd1c-116c-4cfc-9f39-6138fa1f1c5b', 'full_address' => 'Озерный пер., д. 13 кв.193'],
            ['id' => '647f8a86-87a5-4a07-8dbc-d37ec0534524', 'full_address' => 'Березовая ул., д. 19 кв.75'],
            ['id' => '436e25f2-aa61-41f3-969e-69ea0660e84b', 'full_address' => 'Севернаяул., д. 18 кв.37'],
            ['id' => '53a0391a-d37b-423d-b425-ca774a23aeeb', 'full_address' => 'Дачная ул., д. 8 кв.3'],
            ['id' => '0b5ba09f-4404-4243-a03e-f21b8552bd14', 'full_address' => 'Пролетарская ул., д. 20 кв.141'],
            ['id' => '68065abf-aac2-4527-a884-a9909a3f012f', 'full_address' => 'Зеленая ул., д. 13 кв.210'],
            ['id' => '243769a2-a6b9-4195-904e-da2580ded50c', 'full_address' => 'Белорусская ул., д. 12 кв.192'],
            ['id' => '33ffb1d0-d72a-4072-9efe-2dc700b68b2a', 'full_address' => 'Приозерная ул., д. 25 кв.189'],
            ['id' => 'e640584e-71c5-4608-8f13-777b5709930e', 'full_address' => 'Дзержинского ул., д. 25 кв.174'],
            ['id' => '7fd4ad4a-5742-4bd1-9283-2a7aa6845dc2', 'full_address' => 'Железнодорожная ул., д. 18 кв.101'],
            ['id' => '40e98ea8-8ae1-412f-b3c4-9f4d771e44d5', 'full_address' => 'Чкалова ул., д. 16 кв.61'],
        ]);
    }
}
