<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert(
            [
                ['id' => 'bc2b3b02-837c-46e6-9311-071c59e9a06e', 'user_id' =>'117526aa-3077-4a32-a331-a5e4a46f1433', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '8b5ab285-c552-44ba-be16-9ff63aa69495', 'user_id' =>'14790c40-c6ce-48f4-b4ac-eda1bf2eecc7', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '60e5764c-701b-47e5-8c9e-21843a7c2b5b', 'user_id' =>'1de4ac5f-c90e-4607-8ff0-f7a56c780707', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => '5dd93585-69c9-4718-b3e9-d4b600d933d3', 'user_id' =>'21bb5d5f-7320-4fc3-b4f6-dbfebe99b233', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => '10847b1d-92a0-4d77-bdbc-09766ca3278c', 'user_id' =>'2c8efd7c-96b6-43f7-ae38-6cb2c3d87a38', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => 'f36c1ea3-cb12-42eb-a232-a0f090e75fe7', 'user_id' =>'36fc2474-873e-4b11-88c5-57652f42f3b5', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => 'f82493f1-3c3c-44cd-9138-59c7c7eb2d69', 'user_id' =>'37adca82-697b-4a88-af05-1cb8f68b8a5c', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => '584da988-46d7-497f-b19c-ae98e83af6a6', 'user_id' =>'37adca82-697b-4a88-af05-1cb8f68b8a5c', 'role_id' => '921a3a5f-45df-494c-b3a7-2899d8305da0'],
                ['id' => '6263c7bc-012a-4f5f-9d30-fc8985f49911', 'user_id' =>'37adca82-697b-4a88-af05-1cb8f68b8a5c', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => 'f64ba4b4-d2f1-4d5b-9651-f90ed68bb9bd', 'user_id' =>'3937cc8b-156f-4e3d-94cd-e1a6fbd12747', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => '43c514d6-8418-4c90-9943-cd9bc5320042', 'user_id' =>'3937cc8b-156f-4e3d-94cd-e1a6fbd12747', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => 'ebeb904b-fe21-4b7b-bfe3-2e929413545a', 'user_id' =>'3e72f9e6-15fe-455d-b4bb-12408fea1d63', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => 'f9d077ed-4d04-4c1f-9bda-96884b4ecbd1', 'user_id' =>'3e72f9e6-15fe-455d-b4bb-12408fea1d63', 'role_id' => 'a490a596-e15e-4244-a438-212a1b406f73'],
                ['id' => '84544beb-0c52-4201-ad5e-0ec37c70c71c', 'user_id' =>'3e72f9e6-15fe-455d-b4bb-12408fea1d63', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '39445bf0-9778-49f2-9582-0f0788418310', 'user_id' =>'404ccf25-a365-438c-b7a0-ca5e2d57e43b', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => 'f1482143-79d7-40a0-b842-9b74964fbb8c', 'user_id' =>'404ccf25-a365-438c-b7a0-ca5e2d57e43b', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => 'd25070b3-894b-40d3-8e17-ce47967a00f2', 'user_id' =>'48eed20c-4d6b-4bda-b899-d98c9af010c4', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => '7c3cdb78-32a9-4ee6-9375-2d9dd1a4a39c', 'user_id' =>'4a4a95f2-b312-4c70-9fb8-21dab0defbbd', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => 'a4fa523a-65dc-4a44-8029-b1b6386788d2', 'user_id' =>'4d570d39-311b-4ee5-a3ff-652eae618b3c', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => 'ef880450-53ee-4201-8ced-2e8144018ead', 'user_id' =>'50b287ca-b77a-49bb-b5f7-424e898ebf09', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => '6269f7e4-0d20-4dc4-bf04-60caa2c94162', 'user_id' =>'50b83b2a-d476-4486-9a31-bd5c19dc6220', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '0cd11d22-dd0e-4477-a1a7-90acf87faba4', 'user_id' =>'55c05bbf-ffbc-4d73-bf88-b212d058283a', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => '9f7dd192-2f2b-49a8-b360-c94467e86893', 'user_id' =>'55c05bbf-ffbc-4d73-bf88-b212d058283a', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '17b38e18-e537-4839-a6bb-e674d7990fd8', 'user_id' =>'586f87a5-9ff2-4025-af00-9d711f1e1818', 'role_id' => 'a490a596-e15e-4244-a438-212a1b406f73'],
                ['id' => '8985c096-6b2e-4060-962b-1b0eb94574b7', 'user_id' =>'5c5f6e12-9244-41bd-b20d-074eb3211119', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => 'a486a053-2205-48ba-b6e8-4c0e8c1093dc', 'user_id' =>'69ef61f0-3f56-4def-a0bc-fa49f37f0636', 'role_id' => '921a3a5f-45df-494c-b3a7-2899d8305da0'],
                ['id' => 'e31218a0-c5ee-41c8-9423-1a3c4f6a6032', 'user_id' =>'69ef61f0-3f56-4def-a0bc-fa49f37f0636', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '29b7c5d2-06ca-42cf-b5fa-51753a1fd6eb', 'user_id' =>'6b73204d-8b91-459b-983d-61fa849998a5', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => 'ee0ceeb7-6c08-4de5-9759-8c45854957b3', 'user_id' =>'6b73204d-8b91-459b-983d-61fa849998a5', 'role_id' => '921a3a5f-45df-494c-b3a7-2899d8305da0'],
                ['id' => '3a16367c-6bf3-4554-9d21-eef5704766ba', 'user_id' =>'6b73204d-8b91-459b-983d-61fa849998a5', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => 'dad70275-4e81-42b6-a343-33113e96a880', 'user_id' =>'6c1a5df7-1047-4efd-b2e3-569a6c2c0974', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => 'f32de4f5-4360-4b13-a8d1-e0134b91b41a', 'user_id' =>'6e7bfff7-6c5f-4017-bfc3-a1aa8f874c5a', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => '450b6e07-0e41-47b8-a23c-7d36f0d117c4', 'user_id' =>'728086c1-3082-420c-8625-043d171bfc3d', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => 'edd795e1-7144-4d9c-aa77-7e6aec078214', 'user_id' =>'739db128-aa21-4c68-8e7d-d9b8cf915441', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => '4b68e528-e3e0-491a-9cd1-4bba2bffbc8f', 'user_id' =>'794ca20a-9471-46d7-ad23-21765cbd4eb8', 'role_id' => 'a490a596-e15e-4244-a438-212a1b406f73'],
                ['id' => '77cfca44-f353-4282-8118-fb70484e1ecd', 'user_id' =>'8b070603-52de-40ad-b711-675622aa1b8f', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => 'a0d251eb-7b78-43dd-bc14-30b282a27c99', 'user_id' =>'9b145739-8131-43bd-a527-ddd546eee05e', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => 'ed0f9ba8-925b-4ba1-8d14-c0165338b862', 'user_id' =>'a44901be-015b-47be-84cb-0ede775a6526', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => '31c4ecf5-7c53-49e9-b35c-b6728e149fe8', 'user_id' =>'a4b8d0d2-b24a-4e43-b0d3-6ccd05032bd4', 'role_id' => 'a490a596-e15e-4244-a438-212a1b406f73'],
                ['id' => 'a724635b-e01b-4c8e-b2db-c57ae1ba4275', 'user_id' =>'a52f108b-122e-400a-9531-24d7d625bc4f', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => 'f64ed125-70b1-4ad0-b8f5-4072521b8555', 'user_id' =>'aa62560e-791c-49b1-89ed-7d84e0e12037', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => '828f0631-476d-4eea-83a2-f3303db868f7', 'user_id' =>'ab2e9685-7f38-4633-8369-55d76895a52c', 'role_id' => 'a490a596-e15e-4244-a438-212a1b406f73'],
                ['id' => 'caa97196-2a26-4fa6-8e5b-3365b1750e0e', 'user_id' =>'ac411421-5440-4e76-b9e0-2c831a8ac22a', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => '4a7f1c47-38b6-4210-8a73-6b4bdd7d4597', 'user_id' =>'af1596f8-c691-4f61-8876-41b6fdb7a07a', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '8a8f526c-e3e8-49b6-a7c0-875b3d7806e1', 'user_id' =>'b2f3514e-6255-4082-98fe-3d01f2638e67', 'role_id' => '907468ff-1327-406c-9027-8353a5fdf8ad'],
                ['id' => 'e7f84c13-e707-4335-b1a0-a6ec4fe1d016', 'user_id' =>'b3e83a69-49f4-46a1-8848-2db7b25a7b23', 'role_id' => 'a490a596-e15e-4244-a438-212a1b406f73'],
                ['id' => '52197161-341e-48a6-934a-980058c9fe41', 'user_id' =>'b951a693-367f-46c6-bb89-cb219769e89d', 'role_id' => '7259fefb-c02e-4475-a07f-92c7a6de4f5a'],
                ['id' => 'd3d56d65-6220-41c6-b30b-f38e2c009005', 'user_id' =>'ca6635de-7755-4763-9db8-0247da91d49a', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => '08268973-5503-48dc-9269-fed9c84f257d', 'user_id' =>'ca6635de-7755-4763-9db8-0247da91d49a', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '42970ae6-8297-4a8b-b896-4d1f089bfb02', 'user_id' =>'d74f8502-a167-4454-abe2-ad065d102a4c', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => '5d3c3d00-07ab-4251-8a3d-aeaf53fa68c1', 'user_id' =>'d74f8502-a167-4454-abe2-ad065d102a4c', 'role_id' => '442ab61d-ec90-480a-9c40-a48247c2a164'],
                ['id' => '841bf008-090d-4825-8843-de21f07c65cd', 'user_id' =>'f1cf4734-06d0-4be8-97da-d898fe522b47', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
                ['id' => '82bc0c56-1629-43d3-a033-57cfb9438489', 'user_id' =>'f4d2d3b8-fd38-4c50-a722-67bb85e5a68a', 'role_id' => '392e3b2b-9c49-442e-8a5c-f251fef2aea8'],
            ]
        );
    }
}
