<?php
 namespace App\Enums;

 enum GenderEnum:string {
     case Female = 'female';
     case Male = 'male';
 }
