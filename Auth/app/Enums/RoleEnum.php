<?php
namespace App\Enums;

enum RoleEnum:string {
    case Customer = 'customer';
    case Courier = 'courier';
    case Manager = 'manager';
    case Cook = 'cook';
    case Admin = 'admin';
    case Moderator = 'moderator';
}
