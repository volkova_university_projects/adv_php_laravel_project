<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserWithFilterResource;
use App\Models\Role;
use App\Models\User;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Exception;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {

        if (empty(request()->filter)) {
            $query = User::all();
        } else {
            $query = User::usersWithFilter(request()->filter);
        }
        $users = UserWithFilterResource::collection($query);
        return (new PaginationService())->pagination($users, 'users');
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $user = User::query()->where('id', $id)->first();
        if (empty($user)) {
            return response()->json([
               'status' => false,
               'massage' => 'User doesn\'t exist'
            ], 404);
        } else {
            return response()->json([
                $user
            ], 200);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request, string $id): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        $current_user = User::query()
            ->where('id', $id)->first();

        if (empty($current_user)) {
            return response()->json([
               'status' => false,
               'massage' => 'User doesn\'t exist'
            ], 404);
        }

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->full_name)) {
                $current_user->full_name = $data->full_name;
            }
            if (!empty($data->birth_date)) {
                $current_user->birth_date = $data->birth_date;
            }
            if (!empty($data->gender)) {
                $current_user->gender = $data->gender;
            }
            if (!empty($data->phone)) {
                if ($data->phone != $current_user->phone
                    and User::query()->where('phone', $data->phone)->exists()) {
                        return response()->json([
                            'status' => false,
                            'message' => 'This phone already exist'
                        ], 409);
                } else {
                    $current_user->phone = $data->phone;
                }
            }
            if (!empty($data->email)) {
                if ($data->email != $current_user->email
                    and User::query()
                        ->where('email', $data->email)->exists()) {
                            return response()->json([
                                'status' => false,
                                'message' => 'This email already exist'
                            ], 409);
                } else {
                    $current_user->email = $data->email;
                }
            }

            if ($current_user->isDirty()) {
                $current_user->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'User was successfully updated'],
                    'updated_user:' =>
                        new UserResource($current_user)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        if (empty(User::query()->find($id)->id)) {
            return response()->json([
                'status' => false,
                'message' => 'This user doesn\'t exist'
            ], 404);
        } else {
            User::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'User was successfully deleted'
            ], 200);
        }
    }
}
