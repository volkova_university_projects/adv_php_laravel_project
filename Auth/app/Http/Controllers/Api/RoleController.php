<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteRoleRequest;
use App\Http\Requests\UpdateRolesRequest;
use App\Http\Resources\RoleResource;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        $query = Role::all();
        $roles = RoleResource::collection($query);
        return response()->json([
            'roles:' => $roles
        ], 200);
    }


    /**
     * Display a listing of the resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Role::query()->find($id);
        $role = new RoleResource($query);
        return response()->json(
            $role);
    }
}
