<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteRoleRequest;
use App\Http\Requests\UpdateRolesRequest;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

use Illuminate\Validation\Rules\Enum;
use App\Enums\RoleEnum;


class RoleUserController extends Controller
{
    /**
     * Create new roles for user by user_id
     *
     * @param string $user_id
     * @param UpdateRolesRequest $request
     * @return JsonResponse
     */
    public function update(UpdateRolesRequest $request, string $user_id): JsonResponse
    {

        $data = request();

        $current_user = User::query()->where('id', $user_id)->first();

        $unpacked_roles = implode(", ", $data->roles);

        $roles = [
            "manager" => Role::manager(),
            "courier" => Role::courier(),
            "cook" => Role::cook(),
            "admin" => Role::admin(),
            "moderator" => Role::moderator()
        ];
        $errors = [];


        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }
        else {
            foreach ($data->roles as $role){
                if (array_key_exists($role, $roles)) {
                    if (User::withRole($current_user->id, $roles[$role]->id)->doesntExist()) {
                        $current_user->roles()->attach($roles[$role]);
                    } else {
                        $errors[] = $role;
                    }
                }
            }
        }
        if (!empty($errors)){
            return response()->json([
                'status' => false,
                'message' => 'The user already has the ' . implode(", ", $errors) . ' role(s)'
            ], 400);
        }
        return response()->json([
            'status' => true,
            'message' => 'Roles ' . $unpacked_roles . ' were successfully updated',
            'updated_user_roles' => new UserResource($current_user)
        ]);
    }


    /**
     * Delete existing roles for user by user_id
     *
     * @param string $user_id
     * @param DeleteRoleRequest $request
     * @return JsonResponse
     */
    public function delete(DeleteRoleRequest $request, string $user_id): JsonResponse
    {
        $data = request();

//        error_log(request()->roles);

        // подготавливаем данные для валидатора
        $request_roles = ['roles' => explode('|', request()->roles)];

        // валидируем массив по ключу roles.
        //TODO: разобрать как вытащить валидацию в DeleteRoleRequest,
        // почему то при таких же параметрах в файл не поступает массив на валидацию
        $validator = Validator::make($request_roles,
            [
                'roles' => 'required|array|min:1',
                'roles.*' => ['required',' distinct', new Enum(RoleEnum::class)],
            ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $current_user = User::query()->where('id', $user_id)->first();

        // в отличии от прошлой подготовки данных, здесь просто массив
        $unpacked_roles = explode("|", $data->roles);

        // создаем коллекцию ролей, где название роли это ключ
        $roles = Role::all()->keyBy('role')->all();

        // тоже саоме для текущих ролей пользователя
        $user_roles = $current_user->roles->keyBy('role')->all();

        $intersect = array_intersect($unpacked_roles, array_keys($user_roles));

        // если запрашиваемые на удаление роли не существуют у пользователя, то выдаем ошибку со списком ролей
        // на которых свалились
        if (!empty(array_diff($unpacked_roles, $intersect))) {
            return response()->json([
                'status' => false,
                'message' =>  'The user hasn\'t ' . implode(", ", array_diff($unpacked_roles, $intersect)) . ' role(s)'
            ]);
        }

//        error_log(implode(array_intersect($unpacked_roles, array_keys($user_roles))));

//        if (!empty($roles_errors)) {
//            return response()->json([
//               'status' => false,
//               'message' =>  'The user hasn\'t ' . implode(", ", $roles_errors) . ' role(s)'
//            ]);
//        }

        // если все роли есть, то удаляем их у пользователя (проверка на существование пускай пока останется)
        foreach ($unpacked_roles as $role){
            if (User::withRole($current_user->id, $roles[$role]->id)->exists()) {
                $current_user->roles()->detach($roles[$role]);
            }
        }

        // дополнительный запрос как в $current_user из-за бага. возможно просто postman шутит
        $updated_user = User::query()->where('id', $user_id)->first();

        return response()->json([
            'status' => true,
            'message' => 'Role(s) ' . implode(", ", $unpacked_roles) . ' was(were) successfully deleted',
            'updated_user_roles' => new UserResource($updated_user)
        ]);
    }
}
