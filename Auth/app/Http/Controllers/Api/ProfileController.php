<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\ProfileResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Display the current user's profile
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function show(Request $request): JsonResponse
    {
        return response()->json(new ProfileResource(
            User::query()->where('id','=', $request->user()->id)->first()
        ), 200);
    }


    /**
     * Update the current user's profile
     *
     * @param UpdateProfileRequest $request
     * @return JsonResponse
     */
    public function update(UpdateProfileRequest  $request): JsonResponse
    {
        $data = request();

        $current_user = User::query()->where('id','=', $request->user()->id)->first();

        $validator = Validator::make($data->all(), [$request]);

        if (empty($current_user)) {
            return response()->json([
                'status' => false,
                'message' => 'User not found'
            ], 404);
        }

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->full_name)) {
                $current_user->full_name = $data->full_name;
            }
            if (!empty($data->birth_date)) {
                $current_user->birth_date = $data->birth_date;
            }
            if (!empty($data->gender)) {
                $current_user->gender = $data->gender;
            }
            if (!empty($data->phone)) {
                if ($data->phone != $current_user->phone
                    and User::query()->where('phone', $data->phone)->exists()) {
                    return response()->json([
                        'status' => false,
                        'message' => 'This phone already exist'
                    ], 409);
                } else {
                    $current_user->phone = $data->phone;
                }
            }
            if (!empty($data->email)) {
                if ($data->email != $current_user->email
                    and User::query()
                        ->where('email', $data->email)->exists()) {
                    return response()->json([
                        'status' => false,
                        'message' => 'This email already exist'
                    ], 409);
                } else {
                    $current_user->email = $data->email;
                }
            }
            if (!empty($data->password)) {
                $current_user->password = Hash::make($data->password);
            }
            if ($current_user->isDirty()) {
                $current_user->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'Profile was successfully updated'],
                    'updated_profile:' =>
                        new ProfileResource($current_user)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }
}
