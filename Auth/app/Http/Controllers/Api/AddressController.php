<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAddressRequest;
use App\Http\Requests\UpdateAddressRequest;
use App\Http\Resources\AddressAttributes;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use App\Services\PaginationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Address::all();
            $addresses = AddressResource::collection($query);
        } else {
            $addresses = Address::withFilter('addresses', request()->filter);
        }
        return (new PaginationService())->pagination($addresses, 'addresses');
    }


    public function show(string $id): JsonResponse
    {
        $query = Address::query()->where('id', $id)->first();
        if ($query !== null) {
            return response()->json(
                new AddressResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Address not found'
            ], 404);
        }
    }


    /**
     * Create a new Address.
     *
     * @param CreateAddressRequest $request
     * @return JsonResponse
     */
    public function create(CreateAddressRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (Address::query()->where(
                (new AddressAttributes)->attributes($data)
            )->exists()) {
                return response()->json([
                    'status' => false,
                    'message' => 'Duplicate address'
                ], 409);
            } else {
                $new_address = new Address(
                    (new AddressAttributes)->attributes($data));
                $new_address->save();

                return response()->json([
                    'status' => true,
                    'message' => 'Address was successfully created',
                    'uuid' => $new_address->id
                ], 201);
            }
        }
    }

    /**
     * Update an address.
     *
     * @param UpdateAddressRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateAddressRequest $request, string $id): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        $current_address = Address::query()
            ->where('id', $id)->first();

        if (empty($current_address)) {
            return response()->json([
                'status' => false,
                'message' => 'Address not found'
            ], 404);
        }

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->full_address)) {
                $current_address->full_address = $data->full_address;
            }

            if ($current_address->isDirty()) {
                $current_address->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'Address was successfully updated'],
                    'updated_address:' =>
                        new AddressResource($current_address)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }

    /**
     * Delete an address.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_address  = Address::query()->find($id);
        if ($current_address !== null) {
            Address::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Address was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Address doesn\'t exist'
            ], 404);
        }
    }
}
