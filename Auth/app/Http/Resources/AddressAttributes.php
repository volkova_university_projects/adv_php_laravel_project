<?php

namespace App\Http\Resources;

class AddressAttributes
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'full_address' => $request->full_address
        ];
    }
}
