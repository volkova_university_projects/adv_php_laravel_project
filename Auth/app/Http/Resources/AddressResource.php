<?php

namespace App\Http\Resources;

use App\Models\Address;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request): array|\JsonSerializable|Arrayable
    {
        return Address::query()
            ->where('id', $this->id)
            ->select('addresses.id as address_id',
                            'addresses.full_address as full_address'
            )->first();
    }
}
