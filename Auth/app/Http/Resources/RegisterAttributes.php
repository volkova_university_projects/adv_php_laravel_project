<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Hash;

class RegisterAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function attributes($request)
    {
        return [
            'full_name' => $request->full_name,
            'birth_date' => $request->birth_date,
            'gender' => $request->gender,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ];
    }
}
