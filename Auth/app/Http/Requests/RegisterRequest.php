<?php

namespace App\Http\Requests;

use App\Enums\GenderEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class RegisterRequest extends FormRequest
{
//    /**
//     * Determine if the user is authorized to make this request.
//     *
//     * @return bool
//     */
//    public function authorize()
//    {
//        return false;
//    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string',
            'birth_date' => 'required|date_format:Y-m-d',
            'gender' => ['required', new Enum(GenderEnum::class)],
            'phone' => 'required|regex:/^[+]?[0-9]{1,3}\([0-9]{3}\)[0-9]{3}[.-][0-9]{2}[.-][0-9]{2}$/i',
            'email' => 'required|email|unique:App\Models\User,email',
            'password' => 'required|string'
        ];
    }
}
