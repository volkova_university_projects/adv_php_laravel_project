<?php

namespace App\Http\Requests;

use App\Enums\GenderEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'full_name' => 'nullable|string',
            'birth_date' => 'nullable|date_format:Y-m-d',
            'gender' => ['nullable', new Enum(GenderEnum::class)],
            'phone' => 'nullable|regex:/^[+]?[0-9]{1,3}\([0-9]{3}\)[0-9]{3}[.-][0-9]{2}[.-][0-9]{2}$/i',
            'email' => 'nullable|email',
        ];
    }
}
