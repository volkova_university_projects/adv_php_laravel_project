<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Traits\HasRole;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, HasUuids;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'full_name',
        'birth_date',
        'gender',
        'phone',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $primaryKey = 'id';
    protected $keyType = 'string';


    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    protected function address(): HasMany
    {
        return $this->hasMany(Address::class);
    }


    public function authorize_roles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'You are unauthorized!');
    }

    public function hasAnyRole($roles): bool
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('role', $role)->first()) {
            return true;
        }
        return false;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }


    public static function withRole($user_id, $role_id) {
        return User::query()
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'user_roles.role_id', '=', 'roles.id')
            ->where('user_roles.user_id', '=', $user_id)
            ->where('user_roles.role_id', '=', $role_id);
    }



//    public static function userWithRoles($id)
//    {
//        return User::query()
//            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
//            ->join('roles', 'user_roles.role_id', '=', 'roles.id')
//            ->where('users.id', $id)
//            ->get('role');
//    }

    public static function usersWithFilter($filter)
    {

        return User::query()
            ->where('users.full_name', 'ilike', ('%' . $filter . '%'))
            ->orWhere('users.gender', 'ilike',  ($filter . '%'))
            ->orWhere('users.phone', 'ilike', ('%' . $filter . '%'))
            ->orWhere('users.email', 'ilike', ('%' . $filter . '%'))
            ->orWhereRelation('roles', 'role', 'ilike', ('%' . $filter . '%'))
            ->get();

    }


}
