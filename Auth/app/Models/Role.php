<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'roles';

    protected $fillable = ['role'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_roles');
    }

    public static function withAttributes(): Collection|array
    {
        return Role::query()
            ->select(
                'roles.id as role_id',
                        'roles.role as role_name'
            )->get();
    }



    public static function manager() {
        return Role::query()->where('roles.role', '=', 'manager')->first();
    }

    public static function cook() {
        return Role::query()->where('roles.role', '=', 'cook')->first();
    }

    public static function admin() {
        return Role::query()->where('roles.role', '=', 'admin')->first();
    }

    public static function customer() {
        return Role::query()->where('roles.role', '=', 'customer')->first();
    }

    public static function courier() {
        return Role::query()->where('roles.role', '=', 'courier')->first();
    }

    public static function moderator() {
        return Role::query()->where('roles.role', '=', 'moderator')->first();
    }


}
