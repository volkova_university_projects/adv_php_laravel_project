<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Address extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'addresses';

    protected $fillable = ['full_address'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public static function withFilter($entity, $filter): Collection|array
    {
        return Address::query()
            ->where('addresses.full_address', 'ilike', ('%' . $filter . '%'))
            ->select('addresses.id as address_id',
                            'addresses.full_address as full_address'
            )->get();
    }
}
