<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\NotificationStatus;

class Notification extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['user_id', 'order_id', 'status', 'text'];

    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected $casts = [
        'status' => NotificationStatus::class
    ];
}
