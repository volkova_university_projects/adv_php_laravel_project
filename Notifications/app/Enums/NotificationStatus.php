<?php

namespace App\Enums;

//found here https://www.itsolutionstuff.com/post/how-to-use-enum-in-laravelexample.html

enum NotificationStatus:string {
    case New = 'new';
    case Sent = 'sent';
}
