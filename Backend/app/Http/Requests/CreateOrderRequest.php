<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'cart_id' => 'required|string|uuid',
            'address' => 'required|string',
            'price' => 'required|integer',
            'order_time' => 'required|date_format:Y-m-d H:i:s',
            'user_delivery_time' => 'required|date_format:Y-m-d H:i:s',
            'timeslot' => 'required|integer'
        ];
    }
}
