<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'cart_id' => 'nullable|string|uuid',
            'address' => 'nullable|string',
            'price' => 'nullable|integer',
            'order_time' => 'nullable|date_format:Y-m-d H:i:s',
            'user_delivery_time' => 'nullable|date_format:Y-m-d H:i:s',
            'timeslot' => 'nullable|integer'
        ];
    }
}
