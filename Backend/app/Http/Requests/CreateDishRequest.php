<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'dish_name' => 'required|string',
            'category' => 'required|string|exists:categories,id',
            'price' => 'required|integer',
            'description' => 'string',
            'is_vegetarian' => 'required|bool',
            'photo' => 'required|string'
        ];
    }
}
