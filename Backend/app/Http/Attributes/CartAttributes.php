<?php

namespace App\Http\Attributes;


use Illuminate\Http\Request;

class CartAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function attributes($request)
    {
        return [
            'user_id' => $request->user_id
        ];
    }
}
