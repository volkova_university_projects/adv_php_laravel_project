<?php

namespace App\Http\Attributes;

use Illuminate\Http\Request;

class RestaurantAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function attributes($request)
    {
        return [
            'name' => $request->restaurant_name
        ];
    }
}
