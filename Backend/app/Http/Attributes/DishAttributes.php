<?php

namespace App\Http\Attributes;

use Illuminate\Http\Request;

class DishAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function attributes($request)
    {
        return [
            'dish_name' => $request->dish_name,
            'category' => $request->category,
            'price' => $request->price,
            'description' => $request->description,
            'is_vegetarian' => $request->is_vegetarian,
            'photo' => $request->photo
        ];
    }
}
