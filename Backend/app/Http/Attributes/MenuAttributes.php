<?php

namespace App\Http\Attributes;

use Illuminate\Http\Request;

class MenuAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function attributes($request)
    {
        return [
            'name' => $request->menu_name,
            'restaurant_id' => $request->restaurant_id
        ];
    }
}
