<?php

namespace App\Http\Attributes;

use Illuminate\Http\Request;

class OrderAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function attributes($request)
    {
        return [
            'cart_id' => $request->cart_id,
            'address' => $request->address,
            'price' => $request->price,
            'order_time' => $request->order_time,
            'user_delivery_time' => $request->user_delivery_time,
            'timeslot' => $request->timeslot
        ];
    }
}
