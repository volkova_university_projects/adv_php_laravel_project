<?php

namespace App\Http\Attributes;

use Illuminate\Http\Request;

class TimeslotAttributes
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function attributes($request)
    {
        return [
            'name' => $request->timeslot_name,
            'delivery_time' => $request->delivery_time
        ];
    }
}
