<?php


namespace App\Http\Controllers;

use App\Http\Requests\PhotoStoreRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Application|Factory|View
    {
        $photos = Storage::files('photos');
        $photoUrls = [];
        foreach ($photos as $photo) {
            error_log(Storage::url($photo));
            $photoUrls[] = Storage::url($photo);
        }
        return view('index', ['photoUrls' => $photoUrls]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(Request $request): Application|Factory|View
    {
        return view('image_upload', ['photo' => Session::get('photo', 'no data at all')]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param PhotoStoreRequest $request
     * @return RedirectResponse
     */
    public function store(PhotoStoreRequest $request): RedirectResponse
    {
        $photo = request()->file('photo');

        Validator::make([$photo], [$request]);

        $fileName = time() . '_' . $photo->getClientOriginalName();
        error_log($photo);
        error_log($fileName);

        $path = request()->file('photo')->storeAs('photos', $fileName);
        Session::put('photo', Storage::url($path));

        return redirect()->route('create')->withSuccess(__('Photo "' . $fileName . '" uploaded successfully!'));

    }

    //TODO: make delete from storage
}
