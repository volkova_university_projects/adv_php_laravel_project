<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\Http\Attributes\MenuAttributes;
use App\Http\Resources\MenuResource;
use App\Models\Menu;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    /**
     * Show all menu.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Menu::all();
            $carts = MenuResource::collection($query);
        } else {
            $carts = Menu::withFilter('carts', request()->filter);
        }

        return (new PaginationBackendService())->pagination($carts, 'carts');
    }


    /**
     * Show the menu by menu_id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Menu::query()->where('id', $id)->first();
        if($query !== null) {
            return response()->json(
                new MenuResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Menu not found'
            ], 404);
        }
    }



    /**
     * Create a new menu.
     * @param CreateMenuRequest $request
     * @return JsonResponse
     */
    public function create(CreateMenuRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (Menu::query()->where(
                (new MenuAttributes)->attributes($data)
            )->exists()) {
                return response()->json([
                    'status' => false,
                    'message' => 'Duplicate menu'
                ], 409);
            } else {
                $new_menu = new Menu(
                    (new MenuAttributes)->attributes($data)
                );

                $new_menu->save();

                return response()->json([
                    'status' => true,
                    'message' => 'Menu was successfully created',
                    'uuid' => $new_menu->id
                ], 201);
            }
        }
    }

    /**
     * Update the menu by menu_id.
     *
     * @param UpdateMenuRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateMenuRequest $request, string $id): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        $current_menu = Menu::query()->where('id', $id)->first();

        if (empty($current_menu)) {
            return response()->json([
                'status' => false,
                'message' => 'Menu not found'
            ], 404);
        }

        //TODO: убрать else, сделать проверку в цикле
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->menu_name)) {
                $current_menu->name = $data->menu_name;
            }
            if (!empty($data->restaurant_id)) {
                $current_menu->restaurant_id = $data->restaurant_id;
            }
            if ($current_menu->isDirty()) {
                $current_menu->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'Dish was successfully updated'],
                    'updated_menu:' =>
                        new MenuResource($current_menu)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id)
    {
        $current_dish  = Menu::query()->find($id);

        if ($current_dish !== null) {
            Menu::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Menu was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Menu doesn\'t exist'
            ], 404);
        }
    }
}
