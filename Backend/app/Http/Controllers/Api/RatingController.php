<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateRatingRequest;
use App\Http\Resources\RatingResource;
use App\Models\Rating;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Rating::all();
            $ratings = RatingResource::collection($query);
        } else {
            $ratings = Rating::withFilter('orders', request()->filter);
        }

        return (new PaginationBackendService())->pagination($ratings, 'orders');
    }



    /**
     * Display the specified resource.
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Rating::query()->where('id', $id)->first();
        if ($query !== null) {
            return response()->json(
                new RatingResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Rating not found'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRatingRequest  $request
     * @param  string  $id
     * @return JsonResponse
     */
    public function update(UpdateRatingRequest $request, string$id): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        $current_rating = Rating::query()->where('id', $id)->first();

        if (empty($current_rating)) {
            return response()->json([
                'status' => false,
                'message' => 'Dish not found'
            ], 404);
        }

        //TODO: убрать else
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->rating_value)) {
                $current_rating->value = $data->rating_value;
            }
            if ($current_rating->isDirty()) {
                $current_rating->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'Rating was successfully updated'],
                    'updated_rating:' =>
                        new RatingResource($current_rating)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }

}
