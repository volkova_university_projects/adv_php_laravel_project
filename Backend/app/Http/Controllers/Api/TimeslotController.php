<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTimeslotRequest;
use App\Http\Requests\UpdateTimeslotRequest;
use App\Http\Attributes\TimeslotAttributes;
use App\Http\Resources\TimeslotResource;
use App\Models\Timeslot;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class TimeslotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Timeslot::query()
                ->orderBy('timeslots.delivery_time')->get();
            $restaurants = TimeslotResource::collection($query);
        } else {
            $restaurants = Timeslot::withFilter('restaurants', request()->filter);
        }

        return (new PaginationBackendService())->pagination($restaurants, 'restaurants');
    }


    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Timeslot::query()->where('id', $id)->first();
        if ($query !== null) {
            return response()->json(
                new TimeslotResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Cart not found'
            ], 404);
        }
    }


    /**
     * Show the form for creating a new resource.
     * @param CreateTimeslotRequest $request
     * @return JsonResponse
     */
    public function create(CreateTimeslotRequest $request): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (Timeslot::query()->where(
                (new TimeslotAttributes)->attributes($data)
            )->exists()) {
                return response()->json([
                    'status' => false,
                    'message' => "Duplicate timeslot"
                ], 409);
            } else {
                $new_timeslot = new Timeslot(
                    (new TimeslotAttributes)->attributes($data)
                );
//                return response()->json($new_timeslot);

                $new_timeslot->save();

                return response()->json([
                    'status' => true,
                    'message' => 'Timeslot was successfully created',
                    'id' => $new_timeslot->id
                ], 201);
            }
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTimeslotRequest  $request
     * @param  string  $id
     * @return JsonResponse
     */
    public function update(UpdateTimeslotRequest $request, string $id): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        $current_timeslot = Timeslot::query()->where('id', $id)->first();

        //TODO: убрать else, сделать проверку в цикле
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->timeslot_name)) {
                $current_timeslot->name = $data->timeslot_name;
            }
            if (!empty($data->delivery_time)) {
                $current_timeslot->delivery_time = $data->delivery_time;
            }

            if ($current_timeslot->isDirty()) {
                $current_timeslot->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'Timeslot was successfully updated'],
                    'updated_timeslot:' =>
                        new TimeslotResource($current_timeslot)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_dish  = Timeslot::query()->find($id);
        if ($current_dish !== null) {
            Timeslot::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Timeslot was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Timeslot doesn\'t exist'
            ], 404);
        }
    }

}
