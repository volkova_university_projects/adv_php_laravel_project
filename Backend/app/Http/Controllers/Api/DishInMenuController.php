<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\DishInMenuResource;
use App\Models\Dish;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class DishInMenuController extends Controller
{

    /**
     * Show all dishes in menu by menu_id.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show_dishes(string $id): JsonResponse
    {
        //TODO: сделать фильтрацию в меню
        $dishes_in_menu = DishInMenuResource::collection(Dish::getDishByMenu($id));
        return (new PaginationBackendService())->pagination($dishes_in_menu, 'dishes_in_menu');
    }
}
