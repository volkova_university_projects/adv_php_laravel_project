<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;

class DishInCartController extends Controller
{

    /**
     * Display all dishes in cart by cart_id.
     *
     * @return JsonResponse
     */
    public function show_dishes(): JsonResponse
    {
        //TODO: сделать фильтрацию в корзине
        $dishes_in_cart = Dish::getDishesByCart(request()->id);
        return (new PaginationBackendService())->pagination($dishes_in_cart, 'dishes_in_cart');
    }

}
