<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDishRequest;
use App\Http\Requests\UpdateDishRequest;
use App\Http\Attributes\DishAttributes;
use App\Http\Resources\DishResource;
use App\Models\Dish;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class DishController extends Controller
{
    /**
     * Show all dishes.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Dish::all();
            $dishes = DishResource::collection($query);
        } else {
            $dishes = Dish::withFilter('dishes', request()->filter);
        }
        return (new PaginationBackendService())->pagination($dishes, 'dishes');
    }


    /**
     * Show the dish by dish_id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Dish::query()->where('id', $id)->first();
        if ($query !== null) {
            return response()->json(
                new DishResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Dish not found'
            ], 404);
        }
    }


    /**
     * Create a new dish
     *
     * @param CreateDishRequest $request
     * @return JsonResponse
     */
    public function create(CreateDishRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
               'status' => false,
               'message' => $validator->errors()
            ], 400);

        }

        if (Dish::query()->where(
            (new DishAttributes)->attributes($data)
        )->exists()) {
            return response()->json([
                'status' => false,
                'message' => "Duplicate dish"
            ], 409);
        } else {
            $new_dish = new Dish(
                (new DishAttributes)->attributes($data)
            );

            $new_dish->save();

            return response()->json([
                'status' => true,
                'message' => 'Dish was successfully created',
                'uuid' => $new_dish->id
            ], 201);
        }
    }



    /**
     * Update the dish by dish_id
     *
     * @param UpdateDishRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateDishRequest $request, string $id): JsonResponse
    {
        $current_dish = Dish::query()->where("id", $id)->first();

        if (empty($current_dish)) {
            return response()->json([
                'status' => false,
                'message' => 'Dish not found'
            ], 404);
        }

        $dishes = request()->only(
            "dish_name",
            "category",
            "price",
            "description",
            "is_vegetarian",
            "photo"
        );

        $validator = Validator::make($dishes, [$request]);

        if ($validator->fails()) {
            return response()->json([
               'status' => false,
               'message' => $validator->errors()
            ], 400);
        }


        foreach (array_keys($dishes) as $key)
        {
            if (!empty($dishes[$key]) && $current_dish->$key != $dishes[$key]) {
                $current_dish->$key = $dishes[$key];
            }
        }


        if ($current_dish->isDirty()) {

            $current_dish->save();

            return response()->json([
                'response:' =>
                        ['status' => true,
                        'message' => 'Dish was successfully updated'],
                        'updated_dish:' =>
                            new DishResource(Dish::oneDish($current_dish->id))
            ]);
        } else {
            error_log('we are here');
            return response()->json([
                'status' => false,
                'message' => 'No changes'
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_dish  = Dish::query()->find($id);
        if ($current_dish !== null) {
            Dish::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Dish was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Dish doesn\'t exist'
            ], 404);
        }
    }
}
