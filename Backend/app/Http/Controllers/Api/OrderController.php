<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Http\Attributes\OrderAttributes;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Order::query()
                ->orderByDesc('orders.order_time')->get();
            $orders = OrderResource::collection($query);
        } else {
            $orders = Order::withFilter('orders', request()->filter);
        }

        return (new PaginationBackendService())->pagination($orders, 'orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Order::query()->where('id', $id)->first();
        if ($query !== null) {
            return response()->json(
                new OrderResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Cart not found'
            ], 404);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param CreateOrderRequest $request
     * @return JsonResponse
     */
    public function create(CreateOrderRequest $request): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            $new_order = new Order(
                (new OrderAttributes)->attributes($data)
            );
            $new_order->save();
            return response()->json([
                'status' => true,
                'message' => 'Order was successfully created',
                'uuid' => $new_order->id
            ], 201);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderRequest $request
     * @param  string  $id
     * @return JsonResponse
     */
    public function update(UpdateOrderRequest $request, string $id): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        $current_order = Order::query()
            ->where('id', $id)->first();

        if (empty($current_order)) {
            return response()->json([
                'status' => false,
                'message' => 'Order not found'
            ], 404);
        }

        //TODO: убрать else, сделать проверку в цикле
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->cart_id)) {
                $current_order->cart_id = $data->cart_id;
            }
            if (!empty($data->address)) {
                $current_order->address = $data->address;
            }
            if (!empty($data->price)) {
                $current_order->price = $data->price;
            }
            if (!empty($data->order_time)) {
                $current_order->order_time = $data->order_time;
            }
            if (!empty($data->user_delivery_time)) {
                $current_order->user_delivery_time = $data->user_delivery_time;
            }
            if (!empty($data->timeslot)) {
                $current_order->timeslot = $data->timeslot;
            }
            if ($current_order->isDirty()) {
                $current_order->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'Order was successfully updated'],
                    'updated_order:' =>
                        new OrderResource($current_order)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_dish  = Order::query()->where('id', $id)->first();
        if ($current_dish !== null) {
            Order::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Order was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Order doesn\'t exist'
            ], 404);
        }
    }
}


