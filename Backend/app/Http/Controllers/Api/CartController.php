<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCartRequest;
use App\Http\Attributes\CartAttributes;
use App\Http\Resources\CartResource;
use App\Models\Cart;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display all carts.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Cart::all();
            $carts = CartResource::collection($query);
        } else {
            $carts = Cart::withFilter('carts', request()->filter);
        }
        return (new PaginationBackendService())->pagination($carts, 'carts');
    }

    /**
     * Display the cart by cart_id.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Cart::query()->where('id', $id)->first();
        if ($query !== null) {
            return response()->json(
                new CartResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Cart not found'
            ], 404);
        }
    }


    /**
     * Create a new cart.
     *
     * @param CreateCartRequest $request
     * @return JsonResponse
     */
    public function create(CreateCartRequest $request): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        }

        if (Cart::query()->where(
            (new CartAttributes)->attributes($data)
        )->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Duplicate cart'
            ], 409);
        } else {
            $new_cart = new Cart(
                (new CartAttributes)->attributes($data));
            $new_cart->save();

            return response()->json([
                'status' => true,
                'message' => 'Cart was successfully created',
                'uuid' => $new_cart->id
            ], 201);
        }
    }



    /**
     * Delete the cart by cart_id.
     *
     * @param  string   $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $cart  = Cart::query()->find($id);
        if ($cart !== null) {
            Cart::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Cart was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Cart doesn\'t exist'
            ], 404);
        }
    }
}
