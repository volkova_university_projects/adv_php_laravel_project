<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRestaurantRequest;
use App\Http\Requests\UpdateRestaurantRequest;
use App\Http\Attributes\RestaurantAttributes;
use App\Http\Resources\RestaurantResource;
use App\Models\Restaurant;
use App\Services\PaginationBackendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function show_all(): JsonResponse
    {
        if (empty(request()->filter)) {
            $query = Restaurant::all();
            $restaurants = RestaurantResource::collection($query);
        } else {
            $restaurants = Restaurant::withFilter('restaurants', request()->filter);
        }

        return (new PaginationBackendService())->pagination($restaurants, 'restaurants');
    }


    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $query = Restaurant::query()->where('id', $id)->first();
        if ($query !== null) {
            return response()->json(
                new RestaurantResource($query));
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Cart not found'
            ], 404);
        }
    }


    /**
     * Show the form for creating a new resource.
     * @param CreateRestaurantRequest $request
     * @return JsonResponse
     */
    public function create(CreateRestaurantRequest $request): JsonResponse
    {
        $data = request();

        $validator = Validator::make($data->all(), [$request]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (Restaurant::query()->where(
                (new RestaurantAttributes)->attributes($data)
            )->exists()) {
                return response()->json([
                    'status' => false,
                    'message' => "Duplicate restaurant"
                ], 409);
            } else {
                $new_restaurant = new Restaurant(
                    (new RestaurantAttributes)->attributes($data)
                );

                $new_restaurant->save();

                return response()->json([
                    'status' => true,
                    'message' => 'Restaurant was successfully created',
                    'uuid' => $new_restaurant->id
                ], 201);
            }
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRestaurantRequest  $request
     * @param  string  $id
     * @return JsonResponse
     */
    public function update(UpdateRestaurantRequest $request, string $id): JsonResponse
    {
        $data = request();
        $validator = Validator::make($data->all(), [$request]);

        $current_restaurant = Restaurant::query()->where('id', $id)->first();

        //TODO: убрать else
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            if (!empty($data->restaurant_name)) {
                $current_restaurant-> name = $data->restaurant_name;
            }

            if ($current_restaurant->isDirty()) {
                $current_restaurant->save();
                return response()->json([
                    'response:' =>
                        ['status' => true,
                            'message' => 'Restaurant was successfully updated'],
                    'updated_restaurant:' =>
                        new RestaurantResource($current_restaurant)
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'No changes'
                ]);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $current_restaurant  = Restaurant::query()->find($id);
        if ($current_restaurant !== null) {
            Restaurant::destroy($id);
            return response()->json([
                'status' => true,
                'message' => 'Restaurant was successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Restaurant doesn\'t exist'
            ], 404);
        }
    }
}
