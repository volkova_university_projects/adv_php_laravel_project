<?php
//
//namespace App\Http\Controllers;
//
//use App\Models\Photo;
//use Illuminate\Contracts\Foundation\Application;
//use Illuminate\Contracts\View\Factory;
//use Illuminate\Contracts\View\View;
//use Illuminate\Http\Request;
//use Illuminate\Http\Response;
//use Illuminate\Http\UploadedFile;
//use App\Http\Requests\PhotoStoreRequest;
//
//class FilesController extends Controller
//{
//    /**
//     * Display a listing of the resource.
//     *
//     * @return Application|Factory|View
//     */
//    public function index(): View|Factory|Application
//    {
//        $photos = Photo::all();
//
//        return view('photos.index', [
//            'photos' => $photos
//        ]);
//    }
//
//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return Application|Factory|View
//     */
//    public function create(): Application|Factory|View
//    {
//        return view('photos.create');
//    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  Request  $request
//     * @return Response
//     */
//    public function store(Request $request): Response
//    {
//        $fileName = time() . '.'. $request->photo->extension();
//
//        $type = $request->photo->getClientMimeType();
//        $size = $request->photo->getSize();
//
//        $request->photo->move(storage_path('app/photos'), $fileName);
//
//        $new_photo = new Photo([
//            'name' => $fileName,
//            'type' => $type,
//            'size' => $size,
//            'path' => '/storage/photos'
//        ]);
//
//        $new_photo->save();
////        Photo::query()->create([
////            'name' => $fileName,
////            'type' => $type,
////            'size' => $size
////        ]);
//
//        return redirect()->route('files.index')->withSuccess(__('File added successfully.'));
//    }
//
//}
