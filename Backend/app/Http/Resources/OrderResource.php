<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        //TODO: убрать все запросы из ресурса

        $order = Order::one($this->id);

        return  [
            'order_id' => $order->order_id,
            'cart_id' => $order->cart_id,
            'user_id' => $order->user_id,
            'address' => $order->address,
            'price' => $order->price,
            'order_time' => $order->order_time,
            'user_delivery_time' => $order->user_delivery_time,
            'timeslot' => $order->timeslot,
            'system_delivery_time' => $order->system_delivery_time,
            'result_delivery_time' => $order->result_delivery_time,
        ];
    }
}
