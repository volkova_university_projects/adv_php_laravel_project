<?php

namespace App\Http\Resources;

use App\Models\Dish;
use Illuminate\Http\Resources\Json\JsonResource;

class DishInCartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //TODO: убрать все запросы из ресурса

        $dish = Dish::inCart($this->id, $this->id);

        return [
          'dish_name' => $dish->dish_name,
            'price' => $dish->price,
            'count' => $dish->count
        ];
    }
}
