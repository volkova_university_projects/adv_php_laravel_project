<?php

namespace App\Http\Resources;

use App\Models\Rating;
use Illuminate\Http\Resources\Json\JsonResource;

class RatingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //TODO: убрать все запросы из ресурса

        $rating = Rating::one($this->id);

        return [
            'rating_id' => $rating->rating_id,
            'dish_id' => $rating->dish_id,
            'dish_name' => $rating->dish_name,
            'rating_value' => $rating->rating_value,

        ];
    }
}
