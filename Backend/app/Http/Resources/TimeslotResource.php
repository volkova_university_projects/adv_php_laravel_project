<?php

namespace App\Http\Resources;

use App\Models\Timeslot;
use Illuminate\Http\Resources\Json\JsonResource;

class TimeslotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //TODO: убрать все запросы из ресурса

        $timeslot = Timeslot::one($this->id);

        return [
            'timeslot_id' => $timeslot->timeslot_id,
            'timeslot_name' => $timeslot->timeslot_name,
            'delivery_time' => $timeslot->delivery_time,
        ];
    }
}
