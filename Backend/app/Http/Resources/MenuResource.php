<?php

namespace App\Http\Resources;

use App\Models\Menu;
use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //TODO: убрать все запросы из ресурса

        $menu = Menu::one($this->id);

        return [
            'menu_id' => $menu->menu_id,
            'menu_name' => $menu->menu_name,
            'restaurant_id' => $menu->restaurant_id,
        ];
    }
}
