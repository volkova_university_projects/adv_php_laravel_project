<?php

namespace App\Http\Resources;

use App\Models\Restaurant;
use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //TODO: убрать все запросы из ресурса

        $restaurant = Restaurant::one($this->id);

        return [
            'restaurant_id' => $restaurant->restaurant_id,
            'restaurant_name' => $restaurant->restaurant_name,
        ];
    }
}
