<?php

namespace App\Http\Resources;

use App\Models\Cart;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        //TODO: убрать все запросы из ресурса

        $cart = Cart::one($this->id);

        return [
            'cart_id' => $cart->cart_id,
            'cart_number' => $cart->cart_number,
            'user_id' => $cart->user_id
        ];
    }
}
