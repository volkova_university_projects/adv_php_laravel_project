<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DishInMenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'dish_id' => $this->dish_id,
            'dish_name' => $this->dish_name,
            'category' => $this->category,
            'menu_id' => $this->menu_id,
            'menu_name' => $this->menu_name,
            'price' => $this->price,
            'description' => $this->description,
            'is_vegetarian' => $this->is_vegetarian,
            'photo' => $this->photo
        ];
    }
}
