<?php

namespace App\Http\Resources;

use App\Models\Dish;
use Illuminate\Http\Resources\Json\JsonResource;

class DishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'dish_id' => $this->id,
            'dish_name' => $this->dish_name,
            'category' => $this->category,
            'menu' => array_keys($this->menus->keyBy('name')->all()),
            'price' => $this->price,
            'description' => $this->description,
            'is_vegetarian' => $this->is_vegetarian,
            'photo' => $this->photo
        ];
    }
}
