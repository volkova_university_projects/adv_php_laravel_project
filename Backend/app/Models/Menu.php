<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Menu extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['name', 'restaurant_id'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function dish(): BelongsToMany
    {
        return $this->belongsToMany(Dish::class, 'dishes_menus');
    }

    protected function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class);
    }

    //TODO: убедиться, что там где необходимо, запросы выполнены в стиле ORM

    public static function withFilter($entity, $filter)
    {
        return Menu::query()
            ->join('restaurants', 'menus.restaurant_id', '=', 'restaurants.id')
            ->where('menus.name', 'ilike', ('%' . $filter . '%'))
            ->select(
                'menus.id as menu_id',
                'menus.name as menu_name',
                'restaurants.id as restaurant_id'
            )->get();
    }

    public static function one($id)
    {
        return Menu::query()
            ->join('restaurants', 'menus.restaurant_id', '=', 'restaurants.id')
            ->where('menus.id', '=', $id)
            ->select(
                'menus.id as menu_id',
                    'menus.name as menu_name',
                    'restaurants.id as restaurant_id'
            )->first();
    }


//    public static function getDishByMenu($id)
//    {
//        return Dish::query()
//            ->join('dishes_menus', 'dishes_menus.dish_id', '=', 'dishes.id')
//            ->join('categories', 'dishes.category', '=', 'categories.id')
//            ->join('menus', 'dishes_menus.menu_id', '=', 'menus.id')
//            ->where('dishes_menus.menu_id',  '=', $id)
//            ->get();
//    }
}
