<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Restaurant extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['name'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    protected function menu(): HasMany
    {
        return $this->hasMany(Menu::class);
    }

    //TODO: убедиться, что там где необходимо, запросы выполнены в стиле ORM

    public static function withFilter($entity, $filter) {
        $restaurants = Restaurant::query()
            ->where('restaurants.name', 'ilike', ('%' . $filter . '%'));

        return
            $restaurants->select(
                'restaurants.id as restaurant_id',
                'restaurants.name as restaurant_name'
            )->get();
    }

    public static function one($id)
    {
        return Restaurant::query()
            ->where('id', '=', $id)
            ->select(
                'restaurants.id as restaurant_id',
                         'restaurants.name as restaurant_name'
            )->first();
    }
}
