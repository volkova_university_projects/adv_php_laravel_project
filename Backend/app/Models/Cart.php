<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Cart extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['cart_number', 'user_id'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';


    protected function user(): HasOne
    {
        return $this->HasOne(User::class);
    }

    protected function dish(): BelongsToMany
    {
        return $this->belongsToMany(Dish::class);
    }

    protected function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }


    //TODO: убедиться, что там где необходимо, запросы выполнены в стиле ORM

    public static function withFilter($entity, $filter)
    {
        $carts = Cart::query()
            ->where('carts.cart_number', 'ilike', ('%' . $filter . '%'))
            ->select(
                'carts.id as cart_id',
                'carts.cart_number as cart_number',
                'carts.user_id as user_id'
            )->get();

        return $carts;
    }

    public static function one($id)
    {
        return Cart::query()
            ->where('carts.id', '=', $id)
            ->select(
                'carts.id as cart_id',
                'carts.cart_number as cart_number',
                'carts.user_id as user_id')
            ->first();
    }

}
