<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Timeslot extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'delivery_time'];

    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    //TODO: убедиться, что там где необходимо, запросы выполнены в стиле ORM

    public static function withFilter($entity, $filter) {

        $timeslots = Timeslot::query()
            ->where('timeslots.name', 'ilike', ('%' . $filter . '%'))
            ->orderBy('timeslots.delivery_time')
            ->select(
                'timeslots.id as timeslot_id',
                'timeslots.name as timeslot_name',
                'timeslots.delivery_time as delivery_time'
            )->get();

        return $timeslots;
    }

    public static function one($id)
    {
        return Timeslot::query()
            ->where('timeslots.id', '=', $id)
            ->select(
                'timeslots.id as timeslot_id',
                        'timeslots.name as timeslot_name',
                        'timeslots.delivery_time as delivery_time'
            )->first();
    }
}
