<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['cart_id', 'order_time',
        'user_delivery_time', 'system_delivery_time', 'result_delivery_time', 'price', 'address', 'timeslot'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function cart(): HasOne
    {
        return $this->HasOne(Cart::class);
    }

    protected function restaurant(): HasOne
    {
        return $this->hasOne(Restaurant::class);
    }

    protected function user(): HasOne
    {
        return $this->hasOne(User::class);
    }

    protected function timeslot(): HasOne
    {
        return $this->hasOne(Timeslot::class);
    }

    //TODO: убедиться, что там где необходимо, запросы выполнены в стиле ORM

    public static function withFilter($entity, $filter) {
        $orders = Order::query()
            ->join('carts', 'orders.cart_id', '=', 'carts.id')
            ->join('timeslots', 'orders.timeslot', '=', 'timeslots.id')
            ->where('orders.address', 'ilike', ('%' . $filter . '%'))
            ->orWhere('timeslots.name', 'ilike', ('%' . $filter . '%'))
            ->orderBy('orders.order_time', 'desc')
            ->select(
                'orders.id as order_id',
                        'carts.id as cart_id',
                        'carts.user_id as user_id',
                        'orders.address as address',
                        'orders.price as price',
                        'orders.order_time as order_time',
                        'orders.user_delivery_time as user_delivery_time',
                        'timeslots.name as timeslot',
                        'orders.system_delivery_time as system_delivery_time',
                        'orders.result_delivery_time as result_delivery_time'
            )->get();
        return $orders;
    }

    public static function one($id)
    {
        return Order::query()
            ->join('carts', 'orders.cart_id', '=', 'carts.id')
            ->join('timeslots', 'orders.timeslot', '=', 'timeslots.id')
            ->where('orders.id', '=', $id)
            ->select(
                'orders.id as order_id',
                        'carts.id as cart_id',
                        'carts.user_id as user_id',
                        'orders.address as address',
                        'orders.price as price',
                        'orders.order_time as order_time',
                        'orders.user_delivery_time as user_delivery_time',
                        'timeslots.name as timeslot',
                        'orders.system_delivery_time as system_delivery_time',
                        'orders.result_delivery_time as result_delivery_time'
            )->first();
    }
}
