<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Rating extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['dish_id', 'user_id', 'value'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function dish(): HasOne
    {
        return $this->hasOne(Dish::class);
    }

    protected function user(): HasOne
    {
        return $this->hasOne(User::class);
    }


    //TODO: убедиться, что там где необходимо, запросы выполнены в стиле ORM

    public static function withFilter($entity, $filter): Collection|array
    {

        $ratings = Rating::query()
            ->join('dishes', 'ratings.dish_id', '=', 'dishes.id')
            ->where('dishes.name', 'ilike', ('%' . $filter . '%'))
            ->orWhere('ratings.value', 'ilike', $filter)
            ->select(
                'ratings.id as rating_id',
                'dishes.id as dish_id',
                'dishes.name as dish_name',
                'ratings.value as rating_value'
            )->get();

        return $ratings;
    }

    public static function one($id)
    {
        return Rating::query()
            ->join('dishes', 'ratings.dish_id', '=', 'dishes.id')
            ->where('ratings.id', '=', $id)
            ->select(
                'ratings.id as rating_id',
                        'dishes.id as dish_id',
                        'dishes.name as dish_name',
                        'ratings.value as rating_value'
            )->first();
    }
}
