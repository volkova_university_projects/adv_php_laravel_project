<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Category extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['category'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function dish(): BelongsTo
    {
        return $this->belongsTo(Dish::class, );
    }
}
