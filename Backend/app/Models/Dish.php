<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Dish extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['dish_name', 'category', 'price', 'description', 'is_vegetarian', 'photo'];
    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected function cart(): BelongsToMany
    {
        return $this->belongsToMany(Cart::class);
    }

    protected function rating(): BelongsTo
    {
        return $this->belongsTo(Rating::class);
    }


    protected function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category');
    }

    protected function menus(): BelongsToMany
    {
        return $this->belongsToMany(Menu::class, 'dishes_menus');
    }

    protected function file(): BelongsToMany
    {
        return $this->belongsToMany(File::class, 'files_dishes');
    }

    //TODO: убедиться, что там где необходимо, запросы выполнены в стиле ORM

    public static function withFilter($entity, $filter) {

        return Dish::query()
            ->join('dishes_menus', 'dishes.id', '=', 'dishes_menus.dish_id')
            ->join('menus', 'dishes_menus.menu_id', '=', 'menus.id')
            ->join('categories', 'dishes.category', '=', 'categories.id')
            ->where('dishes.dish_name', 'ilike', ('%' . $filter . '%'))
            ->orWhere('dishes.category', 'ilike', ('%' . $filter . '%'))
            ->select(
                'dishes.id as dish_id',
                'dishes.dish_name as dish_name',
                'categories.category as category',
                'menus.id as menu_id',
                'dishes.price as price',
                'dishes.description as description',
                'dishes.is_vegetarian as is_vegetarian',
                'dishes.photo as photo'
            )->get();
    }

    public static function getDishesByCart($id): Collection|array
    {
        return Dish::query()
            ->join('carts_dishes', 'dishes.id', '=', 'carts_dishes.dish_id')
            ->where('carts_dishes.cart_id', $id)
            ->select(
            'dishes.id as dish_id',
                    'dishes.dish_name as dish_name',
                    'dishes.price',
                    'carts_dishes.count as count'
        )->get();
    }

//    public static function getDishByMenu($id)
//    {
//        return Dish::query()
//            ->join('dishes_menus', 'dishes_menus.dish_id', '=', 'dishes.id')
//            ->join('categories', 'dishes.category', '=', 'categories.id')
//            ->join('menus', 'dishes_menus.menu_id', '=', 'menus.id')
//            ->where('dishes_menus.menu_id',  '=', $id)
//            ->get();
//    }


    public static function getDishByMenu($id)
    {
        return Dish::query()
            ->join('dishes_menus', 'dishes_menus.dish_id', '=', 'dishes.id')
            ->join('categories', 'dishes.category', '=', 'categories.id')
            ->join('menus', 'dishes_menus.menu_id', '=', 'menus.id')
            ->where('dishes_menus.menu_id',  '=', $id)
            ->select(
                'dishes.id as dish_id',
                'dishes.dish_name as dish_name',
                'categories.category as category',
                'dishes_menus.menu_id as menu_id',
                'menus.name as menu_name',
                'dishes.price as price',
                'dishes.description as description',
                'dishes.is_vegetarian as is_vegetarian',
                'dishes.photo as photo')
            ->get();
    }




    public static function oneDish($id)
    {
        return Dish::query()
            ->where('dishes.id', $id)
            ->first();
    }

    public static function inCart($dish_id, $cart_id)
    {
        return Dish::query()
            ->join('carts_dishes', 'dishes.id', '=', 'carts_dishes.dish_id')
            ->where('dishes.id', $dish_id)
            ->where('carts_dishes.cart_id', $cart_id)
            ->select(
            'dishes.id as dish_id',
                    'dishes.dish_name as dish_name',
                    'dishes.price',
                    'carts_dishes.count as count'
            )->first();
    }
}
