<?php

namespace App\Services;

use Illuminate\Http\JsonResponse;
class PaginationBackendService
{
    public function pagination($data, $entity) : JsonResponse
    {
        $currentPage = request()->query('page') ?: 1;
        $pageSize = request()->query('size') ?: 10;
        $itemCollection = collect($data);
        $pageCount = ceil(count($data) / $pageSize);


        $currentPageItems = $itemCollection->slice(($currentPage * $pageSize) - $pageSize, $pageSize)->values()->all();


        if (count($currentPageItems)) {
            return response()->json([
                'pageInfo' => [
                    'pageSize' => intval($pageSize),
                    'pageCount' => intval($pageCount),
                    'currentPage' => intval($currentPage)
                ],
                $entity => $currentPageItems
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Page not found'
            ], 404);
        }
    }
}
