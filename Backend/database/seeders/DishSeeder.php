<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dishes')->insert([
            [
                'id' => 'e3ac4eec-95e7-4f5b-9d86-b8b282118ca7',
                'dish_name' => 'Лапша с говядиной по-сычуаньски',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 530.0,
                'description' => 'одно из самых популярных блюд в Китае. По вкусу напоминающее итальянскую пасту, а по подаче схоже с лапшой удон.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/lapsha_s_govyadinoy_po_cichuanski.jpg'
            ],
            [
                'id' => 'be75e611-c9eb-46dd-8ccd-18d485ea6ed3',
                'dish_name' => 'Лапша со свининой Шанхай',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 378.0,
                'description' => 'наваристая лапша из свинины с добавлением соевой пасты.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/lapsha_so_svininoy_shanhai.jpg'
            ],
            [
                'id' => '8f095579-2d1a-4269-9897-cb5db83853f4',
                'dish_name' => 'Лапша 1000 лет здоровья',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 250.0, 'description' => 'суп из лапши, в котором лапша не варится, а только томится.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/lapsha_1000_let_sdorovia.jpg'
            ],
            [
                'id' => 'b81cf3e5-eb1e-4d11-859d-f6cee5997e3f',
                'dish_name' => 'Лапша с грибами шиитаке',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 472.0,
                'description' => 'лапша, приготовленная с добавлением грибов шиитаки.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/lapsha_s_gribami_shiitake.jpg'
            ],
            [
                'id' => '084fbd41-3d9e-4863-af5d-ac7b10f04b81',
                'dish_name' => 'Лапша с курочкой по-кантонски',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 353.0,
                'description' => 'блюдо китайской кухни, которое готовится из лапши, курицы, сои, лука, перца, соевого соуса, масла, крахмала, специй, и подается с зеленым перцем.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/lapsha_s_kyrochkoi_po_kantonski.jpg'
            ],
            [
                'id' => '34fcf7eb-465e-4719-aee7-b8c360d691b7',
                'dish_name' => 'Лапша с говядиной и капустой кимчи',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 200.0, 'description' => 'Нечто, приготовленное из лапши, говядины и капусты кимчи.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/lapsha_s_govyadinoi_i_kapystoi_kimchi.jpg'
            ],
            [
                'id' => 'f9ee287b-0eb8-4aae-964a-c285864f0c61',
                'dish_name' => 'Лапша, жареная на воке с морепродуктами и соусом Чоу мейн',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 737.0,
                'description' => 'блюдо из лапши и морепродуктов, которое считается национальным китайским блюдом.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/lapsha_jarennaya_na_voke_s_moreproductami.jpg'
            ],
            [
                'id' => '6273cafe-6c9b-4205-9217-7554a55657df',
                'dish_name' => 'Вок с индейкой и желтым карри',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 298.0,
                'description' => 'это не что иное, как вок с шаурмой и шаурма с воком.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/vok_s_indeikoi_i_jeltim_karri.jpg'
            ],
            [
                'id' => '76886f61-fcfc-480e-ad9e-1ad859c630c0',
                'dish_name' => 'Вок Уми Гохан',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 871.0,
                'description' => 'это не просто японская картошка, это картошка с овощами, приготовленная вок.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/vok_umi_gohan.jpg'
            ],
            [
                'id' => 'aacc2939-63ee-49bf-9a57-07b88ff42748',
                'dish_name' => 'Вок с кальмаром и шпинатом',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 273.0,
                'description' => 'это суп, в котором можно найти кальмар, шпинат и много других вкусняшек.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/vok_s_kalmarom_i_shpinatom.jpg'
            ],
            [
                'id' => 'e60cc63c-17d5-47cb-bb36-2b83dc561619',
                'dish_name' => 'Вок овощной',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 441.0,
                'description' => 'это не только блюдо из капусты, но и то, что делается из капусты.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/vok_ovoshnoi.jpg'
            ],
            [
                'id' => '3087c9fa-dfa9-4045-b6d0-ac38bde19309',
                'dish_name' => 'Вок Карбонара',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 516.0,
                'description' => 'это не макароны с сыром, запеченные в соусе бешамель, а блюдо итальянской кухни, представляющее собой обжаренные овощи, макароны и мясной соус.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/vok_karbonar.jpg'
            ],
            [
                'id' => '1f0881f3-d470-48b3-b2f2-a93db1cd3863',
                'dish_name' => 'Вок с креветками',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 676.0,
                'description' => 'это как борщ, только вкуснее.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/vok_s_krevetkami.jpg'
            ],
            [
                'id' => '03c1910a-7035-49f9-b614-b5d109225f6b',
                'dish_name' => 'Вок со свининой и ананасами',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 487.0,
                'description' => 'это такая каша, в которой много всего, но все же каша.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/vok_so_svininoi_i_ananasami.jpg'
            ],
            [
                'id' => '4f617ae9-6251-4935-94d8-69dffc19a236',
                'dish_name' => 'Вок Удон с курицей',
                'category' => 'e04740ba-200b-4605-bcb3-1595969e2c61',
                'price' => 361.0,
                'description' => 'это блюдо, которое состоит из курицы, но на самом деле оно состоит из риса.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/vok_udon_s_kuritsei.jpg'
            ],
            [
                'id' => '45daafce-f772-4fd4-8507-04d1fd5cb270',
                'dish_name' => 'Четыре Сыра',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 435.0,
                'description' => 'это блюдо, которое готовится из четырех видов сыра',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_4_sira.jpg'
            ],
            [
                'id' => 'ec2c25c5-b2d5-43cb-9d9e-356362e3f58f',
                'dish_name' => 'Party BBQ',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 983.0,
                'description' => 'это не просто пикник, но ещё и пицца, шашлык, пиво, костёр, музыка и новые друзья.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_party_bbq.jpg'
            ],
            [
                'id' => '832b42ec-7a81-4477-a9f3-6e39ed28058a',
                'dish_name' => 'А-ля диабло',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 507.0,
                'description' => 'это пицца с мясом, приготовленная в стиле Diablo.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_alya_diablo.jpg'
            ],
            [
                'id' => '93d8329b-06c4-401a-9202-89f53f768ad0',
                'dish_name' => 'Алоха Гавайи',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 854.0,
                'description' => 'это блюдо, которое состоит из пиццы, которая называется Аллоха (Aloha), и которая стоит в два раза дороже, чем настоящая.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_aloha_gavaii.jpg'
            ],
            [
                'id' => 'b37a62dd-c82b-49e4-acb9-b31161882b51',
                'dish_name' => 'Грибная',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 489.0,
                'description' => 'то блюдо, состоящее из грибов, сыра, лука, соуса и сыра.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_gribnaya.jpg'
            ],
            [
                'id' => '6e53da5a-126c-4798-8b43-4008c0fcbbc9',
                'dish_name' => 'Деревенская',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 676.0,
                'description' => 'это пицца, которая не просто вкусная, а вкусная настолько, что вы съедите ее даже, если она упадёт вам на голову.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_derevenskya.jpg'
            ],
            [
                'id' => '39c5eb56-7fdc-4771-a378-fa16aaa7306c',
                'dish_name' => 'Карбонара',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 489.0,
                'description' => 'это: — жареный бекон — сырные шарики — помидоры — чеснок — сыр — майонез.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_karbonara.jpg'
            ],
            [
                'id' => '2f142e3d-0f67-451d-9024-0d3fe01fca79',
                'dish_name' => 'Маргарита',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 410.0,
                'description' => 'это пицца, в которой, кажется, есть все, но нет ничего лишнего.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_margarita.jpg'
            ],
            [
                'id' => '16357e42-37e8-4e2f-8a96-13c5365dc231',
                'dish_name' => 'Пеперони',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 869.0,
                'description' => 'это пицца, в которой пицца — это пицца и пепперони — это пепперони.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_peperoni.jpg'
            ],
            [
                'id' => '02758079-0140-43f5-9542-de688f83e43f',
                'dish_name' => 'Римини',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 360.0,
                'description' => 'это итальянская пицца с итальянскими грибами и итальянским пармезаном.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_remini.jpg'
            ],
            [
                'id' => '7a14dec1-0ef4-434b-9acd-4daba7ccc371',
                'dish_name' => 'Салями',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 875.0,
                'description' => 'это пицца, в которой из всего состава начинки только три ингредиента: колбаса, сыр и оливки. ',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_salyami.jpg'
            ],
            [
                'id' => '9ee2f6b6-9812-49c1-bfd2-d350c28f50c0',
                'dish_name' => 'Овощная с грибами',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 803.0,
                'description' => 'это, если не ошибаюсь, пицца, на которой написано Пицца, а сверху она накрыта листом из огурцов и грибов.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_ovoshnaya_s_gribami.jpg'
            ],
            [
                'id' => '9da09bdc-e7fd-409e-882c-43378c9aeae4',
                'dish_name' => 'С ветчиной и грибами',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 569.0,
                'description' => 'это пицца, в которой сыр расплавлен, а ветчина и грибы порезаны на мелкие кусочки. ',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_s_vetchinoi_i_gribami.jpg'
            ],
            [
                'id' => 'daaa7eff-9694-4e4a-bdb2-691fa29a5ce7',
                'dish_name' => 'Цезарь',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 896.0,
                'description' => 'это пицца, из которой убрали все лишнее, чтобы она была по форме похожа на коробку от пиццы, а сверху положили листья салата, сверху полили соусом и сверху посыпали сыром.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_tsezar.jpg'
            ],
            [
                'id' => '886b8fa4-fa42-4088-ba07-8e3443418f1c',
                'dish_name' => 'Милана',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 796.0,
                'description' => 'это что-то среднее между пиццей и мясным рулетом.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_minana.jpg'
            ],
            [
                'id' => 'c9a07e11-8acb-4519-affb-f8ba9615048c',
                'dish_name' => 'Пицца Vitallica L',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 631.0,
                'description' => 'это пицца из мяса, овощей и морепродуктов. Для тех, кто любит мясо, овощи и морепродукты.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_vitallica_L.jpg'
            ],
            [
                'id' => '6340382d-4818-4789-b79d-b5bae7f053a7',
                'dish_name' => 'Классика Лайт',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 231.0,
                'description' => 'это пицца, если в нее положить то, чего нет в пицце Классике',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_klassika_lite.jpg'
            ],
            [
                'id' => 'ce189957-e317-47bf-9e06-9e9cbbd5d0d6',
                'dish_name' => 'Пицца с креветками кимчи',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 581.0,
                'description' => 'это пицца с креветками и кимчи. А Пицца с кимчи — это кимчи и пицца.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_s_krevetkami_kimchi.jpg'
            ],
            [
                'id' => 'b38c52e9-c8e9-441d-b0fd-60e9fced9c45',
                'dish_name' => 'Пицца Портобелло',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 358.0,
                'description' => 'это пицца с тремя видами мяса, тремя видами сыра и тремя видами помидоров.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_portobello.jpg'
            ],
            [
                'id' => '75c5f195-f13e-4d46-a270-a9de95b312a5',
                'dish_name' => 'ВедУрэ',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 319.0,
                'description' => 'это такая пицца, в которой натерт сыр, а сверху на него положили еще много сыра.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_verude.jpg'
            ],
            [
                'id' => '0f99ece0-767d-4854-a997-25c9ca98a6c9',
                'dish_name' => 'Верде Виво',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 883.0,
                'description' => 'то пицца с ветчиной, помидорами, сыром, грибами и оливками.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_verde_vivo.jpg'
            ],
            [
                'id' => '9b2ec2fe-c588-4509-98ea-80e0e41813e2',
                'dish_name' => 'Дель Маэстро',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 605.0,
                'description' => 'это пицца из трех листов теста, двух помидоров, трех зубчиков чеснока, четырех сырых куриных яиц, четвертинки луковицы, трех столовых ложек оливкового масла, соли и перца.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_del_maestro.jpg'
            ],
            [
                'id' => '2f1b671a-9f98-4ab9-b862-3a4d0eb7298b',
                'dish_name' => 'Капричиосо',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 998.0,
                'description' => 'это пицца, в которой нет ни одной лишней детали.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_kaprochioso.jpg'
            ],
            [
                'id' => 'b322bb9a-8641-4389-9b38-7604e6aa137a',
                'dish_name' => 'Кон Карнэ',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 535.0,
                'description' => 'то пицца в форме рожка с сыром и с кусочками мяса, которую кладут на кусок хлеба, поливают кетчупом, сверху украшают зеленью, и все это едят руками.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_kon_karne.jpg'
            ],
            [
                'id' => 'a24d42b3-158a-4ed3-8e07-45e5455ee294',
                'dish_name' => 'Канол',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 668.0,
                'description' => 'это пицца из жидкого теста, в которое заливается соус, сверху выкладываются ингредиенты и все это запекается в духовке.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/pizza_kanol.jpg'
            ],
            [
                'id' => 'de64fcc3-79f1-424c-a207-c115706bd3e0',
                'dish_name' => 'Марио',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 226.0,
                'description' => 'это: - пицца, которая состоит из томатной пасты, соуса песто, сыра моцарелла и томатного соуса; ',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_mario.jpg'
            ],
            [
                'id' => 'f20889ba-8361-421e-b10e-97a65a7d1ea8',
                'dish_name' => 'Палермо',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 999.0,
                'description' => 'это итальянское блюдо из мяса, овощей, сыра, грибов и грибов.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_palermo.jpg'
            ],
            [
                'id' => 'fda6d295-a7c3-4963-9cef-4aeddb2001a7',
                'dish_name' => 'Родео',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 682.0,
                'description' => 'это когда пицца — это как бы Родео, а все остальное — уже не пицца.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_rodeo.jpg'
            ],
            [
                'id' => 'da715157-170b-4adb-b3b8-9d823c6e22e5',
                'dish_name' => 'Римская',
                'category' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac',
                'price' => 901.0,
                'description' => 'это: пицца, состоящая из кусочков пиццы; пицца с разными видами пиццы.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/pizza_rimskya.jpg'
            ],
            [
                'id' => 'f2f4c04d-8f6e-4684-90b6-ea9f55758817',
                'dish_name' => 'Жгучий навар с розовым перцем и вермутом',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 242.0,
                'description' => 'это суп, которым можно угостить любого, но никто не сможет его съесть.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/soup_jgechi_navar_s_rozovim_pertsem_i_vermutom.jpg'
            ],
            [
                'id' => 'e10773ca-4069-4a0a-936e-5a2a92111fa2',
                'dish_name' => 'Зимний суп с зеленым горошком и яйцом',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123', 'price' => 835.0,
                'description' => 'это суп, который варится на основе бульона, в который добавляется много разных овощей и яиц.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_zimni_soop_s_zelenim_goroshkom_i_yaitsom.jpg'
            ],
            [
                'id' => '23414cba-d465-470a-b16d-d9a41e6fe819',
                'dish_name' => 'Черепаховая новогодняя ботвинья с мидиями',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 433.0,
                'description' => 'это блюдо, состоящее из трех частей, первая из которых называется суп.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_cherepahovaya_novogodnyz_botviniya_s_midiyami.jpg'
            ],
            [
                'id' => 'd4e18a13-aabf-4207-80ac-4f344a35c812',
                'dish_name' => 'Сырное баранье потофе с пельменями и картошкой со сладким бататом',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 374.0,
                'description' => 'это суп, в котором все ингредиенты — сыр, баранья голова, картошка, пельмени, батат — прекрасно сочетаются и создают вкуснейший суп.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_sirnoe_baranie_potophe_s_pelmnyami_i_kartoshkoi_so_sladkim_batatom.jpg'
            ],
            [
                'id' => 'f2f885aa-50d6-4d18-ab4f-87086181748e',
                'dish_name' => 'Сытный суп-биск из грифона с морской солью',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 224.0,
                'description' => 'это суп, в котором мясо грифона не успевает свариться.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_sitni_soop_iz_grophona_s_morskoy_soliyu.jpg'
            ],
            [
                'id' => '68397ca2-63c0-4c31-b74d-8bee65086ec4',
                'dish_name' => 'Заправочная зеленая похлебка из грифона',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 385.0,
                'description' => 'это суп-похлебка, приготовленная из трех частей грифона, одной части орла и одной части павиана. Все это заливается двумя частями воды и варится от пяти до семи часов.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_zapravochnaya_zelenaya_pohlebka_iz_griphona.jpg'
            ],
            [
                'id' => '8e097356-6037-4847-8e5f-a9b38862f7ab',
                'dish_name' => 'Теплое рыбное гаспачо с клецками из мацы с картофелем',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 350.0,
                'description' => 'это суп на основе воды, в которой варился картофель, и с добавлением оливкового масла и мацы.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_teploe_ribnoe_gaspacho_s_klezkami_iz_matsi_s_kartophelem.jpg'
            ],
            [
                'id' => '16f0c104-3dd8-4cee-b61f-55d3a9009f04',
                'dish_name' => 'Заправочный суп-бульон с картошкой и рисом',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 157.0,
                'description' => 'это суп, который может быть сытным, вкусным и полезным. Суп-бульона — суп, в котором бульон варится отдельно от остальной еды, а потом из него варят суп. В отличие от супа, суп-бульк — суп с бульоном.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_zapravichniy_soop_bulion_s_kartoshkoi_i_risom.jpg'
            ],
            [
                'id' => 'b680eb95-e739-40a2-b8fb-cc4998c9176e',
                'dish_name' => 'Приправленная баланда из бычьих хвостов',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 389.0,
                'description' => 'это суп с приправами, приготовленный на мясном бульоне, но без мяса.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_propravlennaya_balanda_iz_bichih_hvostov.jpg'
            ],
            [
                'id' => '523ad8d7-b14e-4629-859e-48c776e39294',
                'dish_name' => 'Крабовое охотничье минестроне',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 286.0,
                'description' => 'это бульон, в который добавляют сушеные крабы, а потом пьют с макаронами. ',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_krabovoe_ohotnichie_minestone.jpg'
            ],
            [
                'id' => 'd85370d1-3192-428f-a4fb-a05b0dbe1373',
                'dish_name' => 'Остывший бульон на жирном бульоне с тархуном',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 424.0,
                'description' => 'это не суп, а бульон с тархунов, натертый с жирным бульоном.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_ostivshi_bulion_na_jirnom_bulione_s_tarhunom.jpg'
            ],
            [
                'id' => 'ff944b90-1c52-4760-9d2c-0491e4a2fae5',
                'dish_name' => 'Ячменная имбирная шулемка с яблоками и сидром',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 404.0, 'description' => 'это суп, который готовится на основе ячменной крупы, воды, имбиря и яблока.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_imbirnaya_shylemka_s_yablokami_i_sidrom.jpg'
            ],
            [
                'id' => '4dce94f2-3b89-4932-be81-ec33dbada199',
                'dish_name' => 'Зимнее целебное варево с шариками из свинины',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 461.0,
                'description' => 'это суп, при приготовлении которого свинина была заморожена, а затем размораживалась и разморозка проходила в холодной воде.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_zimnee_tselebnoe_varevo_s_sharikami_iz_svinini.jpg'
            ],
            [
                'id' => 'ea10bc21-8c7b-4be7-8d94-5a2884cc0c6b',
                'dish_name' => 'Детский диетический суп-напиток из телячьих ножек',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 363.0,
                'description' => 'это суп из... чего вы думаете? Правильно, из ножек. ',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_detski_tselebni_soopchik_iz_telyachih_nojek.jpg'
            ],
            [
                'id' => 'e36e44de-bb59-479b-a4c0-c8c89f0fbaa1',
                'dish_name' => 'Рассольная ботвинья на мясном бульоне',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 175.0,
                'description' => 'это холодная ботвинья, разогретая в супе.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_rassolnaya_botviniya_na_myasnom_bulione.jpg'
            ],
            [
                'id' => '1b8a799f-75b2-409e-94b5-253b6a7e1487',
                'dish_name' => 'Жгучее сытное консоме из бараньей печени',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 429.0,
                'description' => 'это напиток, приготовленный из смеси пива, вина, бульона из баранины и бульона, в который добавили немного алкоголя.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_jguchee_konsone_iz_baraniei_pecheni.jpg'
            ],
            [
                'id' => '3549d07c-ac25-4258-8815-738550b16bfd',
                'dish_name' => 'Настоянная юшка из яблок',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 309.0,
                'description' => 'это смесь яблочного сока, воды, сахара, соли и специй.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_hastoyashaya_ushka_iz_yzblok.jpg'
            ],
            [
                'id' => '1b187d2b-71bc-49cb-83cd-9c42aa0a6228',
                'dish_name' => 'Приправленная лапша из говядины',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 344.0,
                'description' => 'это всего лишь лапша, приправленная супом из говядины.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_propravlennaya_lapsha_iz_govyzdini.jpg'
            ],
            [
                'id' => '9769a764-5cdf-4678-a04c-3b6a04e2800a',
                'dish_name' => 'Традиционная юшка',
                'category' => 'd541e17f-82c7-4c47-9d81-c84a06af9123',
                'price' => 179.0,
                'description' => 'это суп, приготовленный из мяса, овощей и лапши.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/soup_traditsionnaya_ushka.jpg'
            ],
            [
                'id' => '036de633-f3df-41b0-89af-58d555701226',
                'dish_name' => 'Бисквитный Тарт Татен в имбирном сиропе',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 163.0,
                'description' => 'это блюдо французской кухни, которое готовится из бисквитного коржа, пропитанного имбирным сиропом, сверху которого кладется крем, а потом все это запекается в духовке.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_biskvitni_tart_taten_v_imbirnom_sirope.jpg'
            ],
            [
                'id' => '4299f458-43ae-435e-abbc-34f8210e83e8',
                'dish_name' => 'Вишневые печенья с ванильным сиропом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 292.0,
                'description' => 'это — то, что вы получите, если в качестве десерта выберете вишневые печенья и ванильный сироп.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_bishnebie_pecheniya_s_vanilnim_siropom.jpg'
            ],
            [
                'id' => '667d91fe-cb78-49c2-aac5-6cfa284ad8bd',
                'dish_name' => 'Творожно-сметанный круассан с ягодным желе',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 181.0,
                'description' => 'это десерт, состоящий из двух круассанов, завернутых в творожно-сливочный крем, и посыпанный тертым шоколадом.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_tvorozno_yzgodniy_kreassan_s_yagodnim_jele.jpg'
            ],
            [
                'id' => '8d3d13e2-2b47-4d37-abea-3acfac7cfb9a',
                'dish_name' => 'Кремовые эклеры с ягодным сиропом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 497.0,
                'description' => 'это что-то вроде оливье без майонеза.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_kremovie_ekleri_s_yagodnim_siropom.jpg'
            ],
            [
                'id' => '773d6fe4-fe76-4597-a811-f173a83e5ce9',
                'dish_name' => 'Вишневый Тарт Татен с шоколадным сиропом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 241.0,
                'description' => 'это десерт, приготовленный в виде коржей, прослоенных вишневым джемом, политых шоколадным соусом и покрытых сверху вишневой глазурью.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_vishneviy_tart_taten_s_shokoladnim_siropom.jpg'
            ],
            [
                'id' => 'eb7847ce-9b7c-4c8d-b12e-9aee020f9fad',
                'dish_name' => 'Диетические булочки с шоколадным сиропом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 483.0,
                'description' => 'это десерт, после которого не хочется есть ничего, кроме диетических булочек с шоколадным',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_dieticheskie_bulochki_s_shokoladnim_siropom.jpg'
            ],
            [
                'id' => '988b091c-2e25-42ee-bddd-2a76ef8e53cb',
                'dish_name' => 'Лаймовый пончик в сахарной пудре',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 138.0,
                'description' => 'это что-то среднее между конфетой Рафаэлло и Маршмеллоу.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_laimoviy_ponchik-v_saharnoi_pudre.jpg'
            ],
            [
                'id' => '1a4c8a37-275e-4f2f-931d-5d83399c134a',
                'dish_name' => 'Вишневые плюшки с каталонским кремом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 258.0,
                'description' => 'это — десерт, состоящий из вишневых плюшек и каталонского крема. ',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_vishnevie_plushki_s_katalonskim_kremom.jpg'
            ],
            [
                'id' => 'ae2cce8b-a339-4ceb-8c42-a264762db550',
                'dish_name' => 'Сладкий Тарт Татен с анисом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 289.0,
                'description' => 'это пирог, который выпекается на решетке и с которого срезают верхнюю часть, чтобы получить углубление для начинки, при этом верхняя часть пирога остается целой.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_sladkiy_tart_taten_s_anisom.jpg'
            ],
            [
                'id' => '2a94d52c-8304-42fd-8e75-6f19b0b5a3c5',
                'dish_name' => 'Сдобные блинчики со взбитыми сливками',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 266.0,
                'description' => 'это то же самое, что и торт Наполеон. А торт Наполеон — то же, что и Десерт.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_sdobnie_bulochki_so_vsbitimi_slivkami.jpg'
            ],
            [
                'id' => 'c35bbb29-2202-4d96-83cc-0bfefb9e395e',
                'dish_name' => 'Финиковый тарт с вишневым желе',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 361.0,
                'description' => 'это, как правило, тарталетка с мороженым, украшенная сверху кусочками фиников.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_phinikoviy_tart_s_vishnevim_jele.jpg'
            ],
            [
                'id' => '7e2d47e5-77f9-449a-a1b5-38df39d196c1',
                'dish_name' => 'Фиговые бисквиты с баварским кремом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 274.0,
                'description' => 'это десерт из трех видов бисквита: вишневого, апельсинового и бананового. В него входит вишня, апельсин и банан.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_phigovie_biskviti_s_bavarskim_kremom.jpg'
            ],
            [
                'id' => '68d50e3d-9cd0-4274-b6b8-f1ee972c47b6',
                'dish_name' => 'Карамельный клути с мармеладом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 333.0,
                'description' => 'Это десерт, в состав которого входит клубничный джем.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_karamelniy_kluti_s_marmeladom.jpg'
            ],
            [
                'id' => 'fd63670a-1fcb-41cc-8c06-a3b55f1dfcc2',
                'dish_name' => 'Медовый бисквит со сливовым желе',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 419.0,
                'description' => 'это десерт, приготовленный из двух видов теста: бисквита и желе. В десерте бисквит готовится на основе муки, яиц, сахара и сливочного масла. Желе готовится из сахара, воды, желатина и молока.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_medoviy_biskvit_so_clivovim_jele.jpg'
            ],
            [
                'id' => '86dff1f1-c83e-4996-9401-01b189cbbf01',
                'dish_name' => 'Вишневые маффины с ягодной прослойкой',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 369.0,
                'description' => 'это десерт, в котором нет ничего лишнего, а именно: ягоды, вишни, маффинов, прослойки и десерта.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_vishnevie_maffini_s_yagodnoi_prosloikoi.jpg'
            ],
            [
                'id' => '9dbca219-2467-4c3a-9228-1c40d876910f',
                'dish_name' => 'Новогодний тарт с ганашем',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 137.0,
                'description' => 'это десерт, состоящий из нескольких слоев разноцветной начинки или крема, посыпанный сверху шоколадом.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_novogodniy_tart_s_ganshem.jpg'
            ],
            [
                'id' => '10294161-a935-4d4a-a9c4-a70790a8ef6b',
                'dish_name' => 'Праздничные блинчики с кленовым сиропом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 299.0,
                'description' => 'это десерт для тех, кто любит блинчики, но не любит кленовый сироп.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_prazdnichnie_blinchiki_s_klenovim_siropom.jpg'
            ],
            [
                'id' => '6d24c242-cf10-49fa-b2e2-4c1924a7df83',
                'dish_name' => 'Праздничный пудинг в варенье',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 130.0,
                'description' => 'это десерт, в котором вместо варенья используется крем!',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_prazdnichniy_pudding_v_varenie.jpg'
            ],
            [
                'id' => '490d9c42-867c-4304-897b-7c715b4b42b6',
                'dish_name' => 'Шоколадно-мятные блинчики с тмином',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 190.0,
                'description' => 'это просто блинчики, но сверху вы можете добавить немного шоколада и мяты.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_shokoladnie_myatnie_blinchiki_s_tminom.jpg'
            ],
            [
                'id' => 'ce721e32-f8f0-4b8a-9e5d-80ea204b3cd8',
                'dish_name' => 'Фирменный пончик с вишней',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 217.0,
                'description' => 'это не просто пончик, а пончик в форме сердца.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_phirmenniy_ponchik_s_vishnei.jpg'
            ],
            [
                'id' => 'b7875837-4c3e-460e-8e47-2af9d6d6e1d6',
                'dish_name' => 'Тягучие бриоши с ягодами и сметанной заливкой',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 254.0,
                'description' => 'это десерт, который с удовольствием съедают даже те, кто не любит сладкое.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_tyaguchie_brioshi_s_yagodami_i_smetannoi_zalivkoi.jpg'
            ],
            [
                'id' => 'e3738020-a36d-4028-8721-afb1f3fac290',
                'dish_name' => 'Персиковый штоллен с персиком',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 331.0,
                'description' => 'это сочетание сладкого теста, соленого изюма и горького шоколада.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_persikoviy_shtollen_s_persikom.jpg'
            ],
            [
                'id' => 'cf4d0fb5-a3e9-4fd0-a5cf-3ac9e6d4aac5',
                'dish_name' => 'Фиговые плюшки с малиной',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 137.0,
                'description' => 'это не что иное, как десерт в виде нескольких слоев теста и начинки из малины, сверху политый малиновым вареньем.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_phigovie_biskviti_s_bavarskim_kremom.jpg'
            ],
            [
                'id' => '58adcb89-1805-439d-9731-5a5c80ae2829',
                'dish_name' => 'Песочный фондан с грушевым сиропом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 217.0,
                'description' => 'это не просто десерт, а десерт с историей. В 17 веке в этот десерт добавляли сироп из сока груши. Позже французы решили добавить в него еще и шоколад, так появился знаменитый шоколадный фондан.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_pesochniy_fondan_s_grushevim_siropom.jpg'
            ],
            [
                'id' => '7284e3bb-8172-49df-99f4-594031553007',
                'dish_name' => 'Заварной синнабон с вишней',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 217.0,
                'description' => 'это такая вкусная булочка, на которой сверху выложена вишня.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_zavarnoi_cinnabon_s_vishnei.jpg'
            ],
            [
                'id' => '35add921-3e40-4ceb-85b0-6310ccbe3572',
                'dish_name' => 'Песочный баноффи с баварским кремом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 458.0,
                'description' => 'это десерт, который состоит в основном из песочного печенья и крема, но имеет также другие ингредиенты, такие как шоколадная крошка, орешки, изюм или цукаты.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/desert_pesochie_banoffi_s_kremom.jpg'
            ],
            [
                'id' => 'ef22e26a-de05-4aa6-bd0a-87af9c60d1ad',
                'dish_name' => 'Ореховые капкейки с ванильным кремом',
                'category' => '07c84a25-90e8-4b64-9499-930ce060fb94',
                'price' => 454.0,
                'description' => 'это — десерт, состоящий из нескольких видов орехов, часто дополняемых фруктами, ягодами, сливками, сгущённым молоком, сахаром, шоколадом, какао, взбитыми сливками.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/desert_orehovie_kapkeiri_s_vanilnim_kremom.jpg'
            ],
            [
                'id' => '3c82b615-ba02-4613-b840-ad6f58956c70',
                'dish_name' => 'Легкий десертный апероль с лимоном',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 375.0,
                'description' => 'это алкогольный напиток, состоящий из воды, сахара, льда, а также ароматизаторов и красителей.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_deserniy_aperol_s_limonom.jpg'
            ],
            [
                'id' => '7dd2b82c-808d-48ee-8395-7e26c35ba8be',
                'dish_name' => 'Цейлонский чай листовой',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 428.0,
                'description' => 'то смесь черного цейлонского чая и листьев чая байхового крупнолистового.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_tseilonskiy_chai_listovoi.jpg'
            ],
            [
                'id' => '8379964b-17ac-40d2-a7d8-28577aacbd12',
                'dish_name' => 'Сладкий кофе с карамелью',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 158.0,
                'description' => 'это миф, придуманный производителями сладкой газированной воды. На самом деле это напиток, сделанный из кофе, воды и сахара.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_sladkiy_kofe_s_karameliy.jpg'
            ],
            [
                'id' => '752386b8-b2da-4dae-bf01-a179c7d61600',
                'dish_name' => 'Классический аперитив с легкой горчинкой и рюмкой водки',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 137.0,
                'description' => 'это — крепкий алкогольный напиток, в состав которого входит водка и лимонный сок.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_klassicheskiy_apperitive_s_legkoi_gorchinkoi_i_rumkoi_vodki.jpg'
            ],
            [
                'id' => 'c37bfd1a-ff3d-4461-be4b-e9086bc86441',
                'dish_name' => 'Сезонный чай из таинственных трав',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 314.0,
                'description' => 'это напиток, который можно приготовить только в сезон. Так как в остальное время он не растет.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_sezonniy_chai_iz_tainstvennih_trav.jpg'
            ],
            [
                'id' => 'ff4b474b-0f41-4616-b59b-45079e4e7f82',
                'dish_name' => 'Холодный колд-брю с карамелью и орехами',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 169.0,
                'description' => 'это напиток, который содержит в себе алкоголь, сахар, воду, орехи и карамель.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_holodniy_kold_bre_s_karameliy_i_orehami.jpg'
            ],
            [
                'id' => '6b5c6bfc-5aac-4561-b72f-b38348c87e29',
                'dish_name' => 'Одуванчиковый банча из корней лопуха',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 123.0,
                'description' => 'это уникальный продукт, в состав которого входят исключительно натуральные ингредиенты: корень лопуха, сахар, спирт и вода.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_oduvanchikovaya_bancha_iz_kornei_lopuha.jpg'
            ],
            [
                'id' => 'b5370458-662a-4edf-814e-83905adacba0',
                'dish_name' => 'Крепкий раф с шоколадным сиропом',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 188.0,
                'description' => 'это коктейль, состоящий из кофе, сливок и сахара.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_krepkiy_raf_s_shokoladnim_siropom.jpg'
            ],
            [
                'id' => '9535232d-bf96-41d5-b4b5-e969c41ca326',
                'dish_name' => 'Горячий свежезаваренный абсент с кубиками льда',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 191.0,
                'description' => 'это средство для борьбы с депрессией. А также с усталостью, головной болью, синдромом хронической усталости, нервным расстройством, бессонницей, стрессом, переутомлением, а также для повышения аппетита.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_goryachiy_svejezavarenniy_absent_s_kubikami_lda.jpg'
            ],
            [
                'id' => '76bec661-b949-41d4-b6e6-22042d24d904',
                'dish_name' => 'Сезонный пуэр с кусочками имбиря',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 291.0,
                'description' => 'это чай, в котором, чтобы сделать его вкус более ярким, добавляют кусочки имбиря.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_sezonniy_puer_s_kusochkami_imbirya.jpg'
            ],
            [
                'id' => '1d7341f4-49f7-435f-9fe6-8c79088bc083',
                'dish_name' => 'Темный карахильо без добавления сахара',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 178.0,
                'description' => 'это коктейль, в котором крепость алкоголя намного превышает крепость сахара.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_temniy_karahilio_bez_dovableniya_sahara.jpg'
            ],
            [
                'id' => '8b6bd0c7-0e47-41d5-8f75-9f8e81c5c6ee',
                'dish_name' => 'Экзотический чай с мятой',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 133.0,
                'description' => 'то чай, при заваривании которого мята не выделяется из листьев, а остается внутри них.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_ekzoticheskiy_chai_s_myatoi.jpg'
            ],
            [
                'id' => '9caf1251-7f3c-47a4-b1c1-fd68d132950f',
                'dish_name' => 'Слабый меланж с карамельным сиропом',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 281.0,
                'description' => 'это безалкогольный коктейль, который готовится с использованием взбитой смеси сливок, молока, сахара и ванилина.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_slabiy_melanj_s_karamelnim_siropom.jpg'
            ],
            [
                'id' => 'fe34e6f0-86a5-40da-8ed4-d9b5746ad97b',
                'dish_name' => 'Ароматный раф-кофе с миндальным кремом',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 145.0,
                'description' => 'это кофе, сваренный в турке на молоке.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_aromatniy_raf_kofe_s_mindalnim_rkemom.jpg'
            ],
            [
                'id' => '4e806930-30f5-4ce1-ae74-b83da97b3c12',
                'dish_name' => 'Разбавленный чай с кусочками фруктов',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 217.0,
                'description' => 'это — напиток, в котором кусочки фруктов за счет своей формы и размера делают его похожим на чай.',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_razbavlenniy_chai_s_kusochkami_fruktov.jpg'
            ],
            [
                'id' => '4d7f1f1a-0461-4c48-a149-1ee7cab44b79',
                'dish_name' => 'Имбирный глинтвейн',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 121.0,
                'description' => 'это смесь красного вина, пряностей, меда и корня имбиря.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_imbirniy_glintvein.jpg'
            ],
            [
                'id' => '494638c5-4678-4edc-b5c5-60b7884a32aa',
                'dish_name' => 'Игдрассильский чай из корнеплодов',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 1220.0,
                'description' => 'это: — напиток, созданный в начале XX века из корня растения, произрастающего в Швеции, и обладающий свойствами настоящего чая.',
                'is_vegetarian' => true,
                'photo' => '/storage/photos/drink_igdrasilskiy_chai_iz_korneplodov.jpg'
            ],
            [
                'id' => '0c91090e-c739-4f66-92ea-12d0acff69ae',
                'dish_name' => 'Светлый меланж с шоколадным сиропом',
                'category' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c',
                'price' => 161.0,
                'description' => 'это напиток, в котором шоколадный сироп сочетается с темным меланжем. ',
                'is_vegetarian' => false,
                'photo' => '/storage/photos/drink_cvetliy_melanj_s_shokoladnim_kremom.jpg'
            ],
        ]);
    }
}
