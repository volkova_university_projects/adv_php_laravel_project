<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeer::class,
            TimeslotSeeder::class,
            RestaurantSeeder::class,
            MenuSeeder::class,
            DishSeeder::class,
            CartSeeder::class,
            RatingSeeder::class,
            CartDishSeeder::class,
            DishesMenuSeeder::class,
            OrderSeeder::class
        ]);



        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
