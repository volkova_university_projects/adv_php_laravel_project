<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            ['id' => '59c9233d-65a8-4cf1-83ce-9b2864b61ba6', 'name' => 'Повар-Бар Кухня'],
            ['id' => '1bfe802b-2f59-440b-b680-cccac31ba3bd', 'name' => 'Джекина доставка'],
            ['id' => 'bf0db591-748a-484f-95b7-2a428d74cc1b', 'name' => 'Багет омлет'],
            ['id' => '956c9901-dd5b-4411-bb48-e9159fd90f00', 'name' => 'Косатка'],
            ['id' => 'd96c543b-de7c-4692-8e5a-b309e81972de', 'name' => 'Black Fish'],
            ['id' => '084ea8b8-9b06-487e-8d13-173860f5a468', 'name' => 'Безумно'],
            ['id' => '4b0c1d09-1945-4534-8001-2da4d0353dcd', 'name' => 'Гости'],
            ['id' => 'dd8039dd-b31a-4490-98aa-345922a0de90', 'name' => 'Столовая дебют'],
            ['id' => 'b15fd43e-c3d3-456f-9ac5-b7a0d9fdf696', 'name' => 'Сэнсэй'],
            ['id' => 'bbb426ba-8e1e-475c-b9e9-aee84744dded', 'name' => 'Panasia'],
            ['id' => '4335d7c3-6d69-4097-beec-150a47dd74e2', 'name' => 'СчастьЕсть'],
            ['id' => 'f1a93162-2696-4775-9f70-81e2273fdd07', 'name' => 'Кафе вегетарианской кухни'],
            ['id' => '0f7be13c-a452-4d5a-95c0-a0c078e91670', 'name' => 'Домашняя кухня'],
            ['id' => '252dac22-84be-478d-84c8-3949e1ce9606', 'name' => 'Каприз Кейтеринг'],
            ['id' => '4409fe6e-ae9f-4d4f-a246-f6ac49ff8eb7', 'name' => 'Diners'],
            ['id' => '0c50f2c4-7265-4040-992d-0df989d327da', 'name' => 'Галерея'],
        ]);
    }
}
