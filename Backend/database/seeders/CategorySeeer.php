<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['id' => 'e04740ba-200b-4605-bcb3-1595969e2c61', 'category' => 'WOK'],
            ['id' => 'ef6209c7-ad4a-437c-8185-c9166ff8c3ac', 'category' => 'Pizza'],
            ['id' => 'd541e17f-82c7-4c47-9d81-c84a06af9123', 'category' => 'Soup'],
            ['id' => '07c84a25-90e8-4b64-9499-930ce060fb94', 'category' => 'Desert'],
            ['id' => '2fe65836-4607-4505-9ab9-d296f4dd2b7c', 'category' => 'Drink'],
        ]);
    }
}
