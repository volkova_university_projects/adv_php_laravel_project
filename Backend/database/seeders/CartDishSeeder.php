<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CartDishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carts_dishes')->insert([
            ['id' => '564df1db-8674-4c2a-867c-f2b96220db1c', 'cart_id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'dish_id' => '02758079-0140-43f5-9542-de688f83e43f', 'count' => 1],
            ['id' => '9de9b421-b163-46af-af48-d9c04c8f7dc7', 'cart_id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'dish_id' => '036de633-f3df-41b0-89af-58d555701226', 'count' => 1],
            ['id' => '038eeb97-4a3d-475c-b184-95447012ac35', 'cart_id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'dish_id' => '03c1910a-7035-49f9-b614-b5d109225f6b', 'count' => 1],
            ['id' => '016c8512-31ce-4abb-8ad2-95215025faf9', 'cart_id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'dish_id' => '084fbd41-3d9e-4863-af5d-ac7b10f04b81', 'count' => 1],
            ['id' => 'c1719d4b-3b5a-4cf0-84ef-c819c9c88a82', 'cart_id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'dish_id' => '16357e42-37e8-4e2f-8a96-13c5365dc231', 'count' => 1],
            ['id' => 'cb1accb9-c234-4f3f-8c9d-2154a4f89ce2', 'cart_id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'dish_id' => '16f0c104-3dd8-4cee-b61f-55d3a9009f04', 'count' => 1],
            ['id' => 'e0897764-e969-408b-9539-a6e12c3e6231', 'cart_id' => '860210a0-fc8c-4d28-8a36-8678135f3607', 'dish_id' => '1a4c8a37-275e-4f2f-931d-5d83399c134a', 'count' => 1],
            ['id' => '7e8a3f54-a0bc-41ee-aa6b-415039235b69', 'cart_id' => '860210a0-fc8c-4d28-8a36-8678135f3607', 'dish_id' => '1b8a799f-75b2-409e-94b5-253b6a7e1487', 'count' => 1],
            ['id' => '49b6c8c6-2d35-464b-a8a2-df6b2e62684c', 'cart_id' => '860210a0-fc8c-4d28-8a36-8678135f3607', 'dish_id' => '1d7341f4-49f7-435f-9fe6-8c79088bc083', 'count' => 1],
            ['id' => 'd7562334-efba-4053-8c8b-9c874c68af71', 'cart_id' => '860210a0-fc8c-4d28-8a36-8678135f3607', 'dish_id' => '1f0881f3-d470-48b3-b2f2-a93db1cd3863', 'count' => 1],
            ['id' => 'bc292d9b-1b83-420b-b482-14ec99240748', 'cart_id' => '860210a0-fc8c-4d28-8a36-8678135f3607', 'dish_id' => '23414cba-d465-470a-b16d-d9a41e6fe819', 'count' => 1],
            ['id' => '166841ab-537d-4e9b-a4b4-90506c2ad094', 'cart_id' => '9838909a-2207-48ff-a2d6-56191dcdecc2', 'dish_id' => '3087c9fa-dfa9-4045-b6d0-ac38bde19309', 'count' => 2],
            ['id' => '1920d916-84ed-47de-ab71-c5d618599526', 'cart_id' => 'a7d21cbb-28e6-4fb0-a9bc-78a51e2eae9b', 'dish_id' => '34fcf7eb-465e-4719-aee7-b8c360d691b7', 'count' => 2],
            ['id' => '6a1823e2-261d-4196-aacb-7e557cf56d66', 'cart_id' => 'a7d21cbb-28e6-4fb0-a9bc-78a51e2eae9b', 'dish_id' => '3549d07c-ac25-4258-8815-738550b16bfd', 'count' => 2],
            ['id' => '2a3fcdb0-231b-4a19-96c3-0cea7f51a607', 'cart_id' => 'a7d21cbb-28e6-4fb0-a9bc-78a51e2eae9b', 'dish_id' => '35add921-3e40-4ceb-85b0-6310ccbe3572', 'count' => 2],
            ['id' => '66d25154-3ac8-4f5e-9f1c-64a2d71c8e9f', 'cart_id' => 'cbba262c-e07b-4bc6-a950-70a64e45bf65', 'dish_id' => '39c5eb56-7fdc-4771-a378-fa16aaa7306c', 'count' => 2],
            ['id' => '0583bbe1-68f7-4ba8-aadf-6a24560d33aa', 'cart_id' => 'cbba262c-e07b-4bc6-a950-70a64e45bf65', 'dish_id' => '4299f458-43ae-435e-abbc-34f8210e83e8', 'count' => 2],
            ['id' => '09ca16b0-059d-487d-b390-8db56ff42991', 'cart_id' => 'cbba262c-e07b-4bc6-a950-70a64e45bf65', 'dish_id' => '494638c5-4678-4edc-b5c5-60b7884a32aa', 'count' => 2],
            ['id' => '3cb4f9ba-60f1-4800-ac3e-73b8f9208352', 'cart_id' => 'cbba262c-e07b-4bc6-a950-70a64e45bf65', 'dish_id' => '4dce94f2-3b89-4932-be81-ec33dbada199', 'count' => 2],
            ['id' => '86920e50-7889-473b-8c67-6ddd31bbdc29', 'cart_id' => 'de870e6b-4fe6-4b32-b6dd-db40779ec787', 'dish_id' => '4e806930-30f5-4ce1-ae74-b83da97b3c12', 'count' => 2],
            ['id' => '15189720-621c-4c87-9a85-ec1cf31f40f4', 'cart_id' => 'de870e6b-4fe6-4b32-b6dd-db40779ec787', 'dish_id' => '58adcb89-1805-439d-9731-5a5c80ae2829', 'count' => 2],
            ['id' => '95f28161-c64a-4c81-b79c-da1b07bed7b9', 'cart_id' => 'de870e6b-4fe6-4b32-b6dd-db40779ec787', 'dish_id' => '68d50e3d-9cd0-4274-b6b8-f1ee972c47b6', 'count' => 2],
            ['id' => '9f355435-2469-468b-b149-c513a2567a66', 'cart_id' => 'de870e6b-4fe6-4b32-b6dd-db40779ec787', 'dish_id' => '68397ca2-63c0-4c31-b74d-8bee65086ec4', 'count' => 3],
            ['id' => '10ca47da-f13d-411b-b552-32813d37873f', 'cart_id' => 'e1f3fa85-7efc-4fd3-9194-8634a4da0767', 'dish_id' => '6d24c242-cf10-49fa-b2e2-4c1924a7df83', 'count' => 3],
            ['id' => '8a78d38e-a270-4fc9-8750-07b4a43382d7', 'cart_id' => 'e1f3fa85-7efc-4fd3-9194-8634a4da0767', 'dish_id' => '6e53da5a-126c-4798-8b43-4008c0fcbbc9', 'count' => 3],
            ['id' => 'e616cf3f-924f-495e-9d25-8ec028fb5be7', 'cart_id' => 'e1f3fa85-7efc-4fd3-9194-8634a4da0767', 'dish_id' => '7284e3bb-8172-49df-99f4-594031553007', 'count' => 3],
            ['id' => '1d5db605-2308-4e3f-8c88-22f128659385', 'cart_id' => 'e1f3fa85-7efc-4fd3-9194-8634a4da0767', 'dish_id' => '752386b8-b2da-4dae-bf01-a179c7d61600', 'count' => 3],
            ['id' => 'de59a7ab-563d-49a5-a2f4-6314cd32d1fa', 'cart_id' => 'ef81af90-6d30-4951-a81b-2f89d3564cdb', 'dish_id' => '75c5f195-f13e-4d46-a270-a9de95b312a5', 'count' => 3],
            ['id' => '83727400-cdfe-4c6a-b11f-81b6974b7816', 'cart_id' => 'ef81af90-6d30-4951-a81b-2f89d3564cdb', 'dish_id' => '7a14dec1-0ef4-434b-9acd-4daba7ccc371', 'count' => 3],
            ['id' => 'f87f3895-5a5b-4f15-b1b4-0cfcfcaaffea', 'cart_id' => 'ef81af90-6d30-4951-a81b-2f89d3564cdb', 'dish_id' => '773d6fe4-fe76-4597-a811-f173a83e5ce9', 'count' => 3],
            ['id' => '80f31871-1272-4075-b013-9e56a35ff851', 'cart_id' => 'ef81af90-6d30-4951-a81b-2f89d3564cdb', 'dish_id' => '886b8fa4-fa42-4088-ba07-8e3443418f1c', 'count' => 3],
            ['id' => '66f6f63e-f50f-4da3-aec9-dbb0baf98952', 'cart_id' => 'f689f1dc-cc3d-4407-817f-cdd111db208d', 'dish_id' => '8e097356-6037-4847-8e5f-a9b38862f7ab', 'count' => 3],
            ['id' => '4225bd38-03b8-493e-898f-15318d7bc618', 'cart_id' => 'f689f1dc-cc3d-4407-817f-cdd111db208d', 'dish_id' => '93d8329b-06c4-401a-9202-89f53f768ad0', 'count' => 3],
            ['id' => '9bad8ea4-af13-406d-9eca-7e387181e2d1', 'cart_id' => 'f689f1dc-cc3d-4407-817f-cdd111db208d', 'dish_id' => '9769a764-5cdf-4678-a04c-3b6a04e2800a', 'count' => 3],
            ['id' => '1d0b7b17-2117-4754-a30d-940c8c5afbec', 'cart_id' => 'f689f1dc-cc3d-4407-817f-cdd111db208d', 'dish_id' => '9b2ec2fe-c588-4509-98ea-80e0e41813e2', 'count' => 3],
            ['id' => '1778f71a-a201-4cf3-8c1e-c03a73d48c00', 'cart_id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'dish_id' => '988b091c-2e25-42ee-bddd-2a76ef8e53cb', 'count' => 3],
            ['id' => 'c2cdb7a4-4f0a-46e9-bb07-7c318bc588f8', 'cart_id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'dish_id' => '9da09bdc-e7fd-409e-882c-43378c9aeae4', 'count' => 3],
            ['id' => '0a66b82a-e50f-4282-b84b-70f824791701', 'cart_id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'dish_id' => '9dbca219-2467-4c3a-9228-1c40d876910f', 'count' => 3],
            ['id' => 'b61f8385-9b51-4642-bf39-e8dd878275a1', 'cart_id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'dish_id' => 'a24d42b3-158a-4ed3-8e07-45e5455ee294', 'count' => 3],
            ['id' => 'fbd0d614-34e7-4826-ade4-34f57a46bfc7', 'cart_id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'dish_id' => 'b5370458-662a-4edf-814e-83905adacba0', 'count' => 3],
            ['id' => '91b9ec52-dd24-45ed-a953-918cd6617543', 'cart_id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'dish_id' => 'c35bbb29-2202-4d96-83cc-0bfefb9e395e', 'count' => 3],
        ]);
    }
}
