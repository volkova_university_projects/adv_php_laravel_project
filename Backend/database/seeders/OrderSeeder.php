<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            ['id' => 'd1c6ce75-0427-48f0-b4ab-324a5269d30f', 'cart_id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'address' => 'Ленина ул., д. 23 кв.71', 'order_time' => '2023-04-06 9:00:01', 'user_delivery_time' => '2023-04-06 10:30:00', 'timeslot' => 6, 'system_delivery_time' => '2023-04-06 11:00:00', 'result_delivery_time' => '2023-04-06 10:53:56', 'price' => 5546.0],
            ['id' => 'aeddcd15-a5c5-4160-902f-b1fdbfeadb70', 'cart_id' => '860210a0-fc8c-4d28-8a36-8678135f3607', 'address' => 'Новоселов ул., д. 23 кв.111', 'order_time' => '2023-04-06 12:10:01', 'user_delivery_time' => '2023-04-06 14:00:00', 'timeslot' => 13, 'system_delivery_time' => '2023-04-06 14:00:00', 'result_delivery_time' => '2023-04-06 14:05:11', 'price' => 1300.0],
            ['id' => '8cb90ba9-bc8a-422a-ad01-c61054c16e74', 'cart_id' => '9838909a-2207-48ff-a2d6-56191dcdecc2', 'address' => 'Тихая ул., д. 13 кв.128', 'order_time' => '2023-04-06 12:15:01', 'user_delivery_time' => '2023-04-06 17:30:00', 'timeslot' => 20, 'system_delivery_time' => '2023-04-06 18:00:00', 'result_delivery_time' => '2023-04-06 18:20:43', 'price' => 5456.0],
            ['id' => 'ee2b2487-269a-4708-ba3d-25aba3abe339', 'cart_id' => 'a7d21cbb-28e6-4fb0-a9bc-78a51e2eae9b', 'address' => 'Центральная ул., д. 10 кв.119', 'order_time' => '2023-04-06 16:08:15', 'user_delivery_time' => '2023-04-06 20:00:00', 'timeslot' => 25, 'system_delivery_time' => '2023-04-06 21:00:00', 'result_delivery_time' => '2023-04-06 20:43:02', 'price' => 5514.0],
            ['id' => '0c153ae4-8298-4d45-b49c-9b0321fb6246', 'cart_id' => 'cbba262c-e07b-4bc6-a950-70a64e45bf65', 'address' => 'Октябрьский пер., д. 10 кв.91', 'order_time' => '2023-04-07 09:15:01', 'user_delivery_time' => '2023-04-07 12:00:00', 'timeslot' => 9, 'system_delivery_time' => '2023-04-07 12:00:00', 'result_delivery_time' => '2023-04-07 12:01:21', 'price' => 5455.0],
            ['id' => 'd877b31b-ba24-445e-a002-ff67ff3fdd41', 'cart_id' => 'de870e6b-4fe6-4b32-b6dd-db40779ec787', 'address' => 'Калинина ул., д. 1 кв.194', 'order_time' => '2023-04-07 10:01:01', 'user_delivery_time' => '2023-04-07 21:00:00', 'timeslot' => 27, 'system_delivery_time' => '2023-04-07 21:00:00', 'result_delivery_time' => '2023-04-07 20:45:00', 'price' => 5646.0],
            ['id' => '98dd4dfb-b949-4bfa-b778-e7c566926955', 'cart_id' => 'e1f3fa85-7efc-4fd3-9194-8634a4da0767', 'address' => 'Совхозная ул., д. 23 кв.211', 'order_time' => '2023-04-07 14:03:00', 'user_delivery_time' => '2023-04-07 20:00:00', 'timeslot' => 25, 'system_delivery_time' => '2023-04-07 20:00:00', 'result_delivery_time' => '2023-04-07 20:15:00', 'price' => 5646.0],
            ['id' => 'ee45101b-dc1b-43bc-984e-bef860888282', 'cart_id' => 'ef81af90-6d30-4951-a81b-2f89d3564cdb', 'address' => 'Восточная ул., д. 24 кв.1', 'order_time' => '2023-04-07 16:34:00', 'user_delivery_time' => '2023-04-07 18:30:00', 'timeslot' => 22, 'system_delivery_time' => '2023-04-07 19:00:00', 'result_delivery_time' => '2023-04-07 19:03:00', 'price' => 3216.0],
            ['id' => '7209b6f9-52e9-4784-9352-673d6d1c7918', 'cart_id' => 'f689f1dc-cc3d-4407-817f-cdd111db208d', 'address' => 'Октябрьская ул., д. 25 кв.20', 'order_time' => '2023-04-07 18:05:00', 'user_delivery_time' => '2023-04-07 21:30:00', 'timeslot' => 28, 'system_delivery_time' => '2023-04-07 21:30:00', 'result_delivery_time' => '2023-04-07 21:35:00', 'price' => 3546.0],
            ['id' => '6a112c4d-b9ba-4419-9b69-5c0a50fa9061', 'cart_id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'address' => 'Строителей ул., д. 8 кв.201', 'order_time' => '2023-04-08 09:45:00', 'user_delivery_time' => '2023-04-07 11:00:00', 'timeslot' => 7, 'system_delivery_time' => '2023-04-07 11:00:00', 'result_delivery_time' => '2023-04-07 10:59:00', 'price' => 5464.0],
        ]);
    }
}
