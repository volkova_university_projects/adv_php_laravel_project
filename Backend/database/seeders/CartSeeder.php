<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carts')->insert([
            ['id' => 'f689f1dc-cc3d-4407-817f-cdd111db208d', 'cart_number' => 1, 'user_id' => '2c8efd7c-96b6-43f7-ae38-6cb2c3d87a38'],
            ['id' => 'cbba262c-e07b-4bc6-a950-70a64e45bf65', 'cart_number' => 2, 'user_id' => 'f4d2d3b8-fd38-4c50-a722-67bb85e5a68a'],
            ['id' => 'fcb654cf-38df-4d8f-a158-6ed640ddeb6d', 'cart_number' => 3, 'user_id' => '3937cc8b-156f-4e3d-94cd-e1a6fbd12747'],
            ['id' => '6c14d79b-5c29-4483-93ab-fdabae91cba5', 'cart_number' => 4, 'user_id' => 'b951a693-367f-46c6-bb89-cb219769e89d'],
            ['id' => 'ef81af90-6d30-4951-a81b-2f89d3564cdb', 'cart_number' => 5, 'user_id' => '9b145739-8131-43bd-a527-ddd546eee05e'],
            ['id' => 'de870e6b-4fe6-4b32-b6dd-db40779ec787', 'cart_number' => 6, 'user_id' => '6e7bfff7-6c5f-4017-bfc3-a1aa8f874c5a'],
            ['id' => '860210a0-fc8c-4d28-8a36-8678135f3607', 'cart_number' => 7, 'user_id' => '739db128-aa21-4c68-8e7d-d9b8cf915441'],
            ['id' => '9838909a-2207-48ff-a2d6-56191dcdecc2', 'cart_number' => 8, 'user_id' => '1de4ac5f-c90e-4607-8ff0-f7a56c780707'],
            ['id' => 'e1f3fa85-7efc-4fd3-9194-8634a4da0767', 'cart_number' => 9, 'user_id' => '6c1a5df7-1047-4efd-b2e3-569a6c2c0974'],
            ['id' => 'a7d21cbb-28e6-4fb0-a9bc-78a51e2eae9b', 'cart_number' => 10, 'user_id' => 'ca6635de-7755-4763-9db8-0247da91d49a'],
        ]);
    }
}
