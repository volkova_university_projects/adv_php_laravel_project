<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeslotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('timeslots')->insert([
            ['name' => '8:00', 'delivery_time' => '8:00:00'],
            ['name' => '8:30', 'delivery_time' => '8:30:00'],
            ['name' => '9:00', 'delivery_time' => '9:00:00'],
            ['name' => '9:30', 'delivery_time' => '9:30:00'],
            ['name' => '10:00', 'delivery_time' => '10:00:00'],
            ['name' => '10:30', 'delivery_time' => '10:30:00'],
            ['name' => '11:00', 'delivery_time' => '11:00:00'],
            ['name' => '11:30', 'delivery_time' => '11:30:00'],
            ['name' => '12:00', 'delivery_time' => '12:00:00'],
            ['name' => '12:30', 'delivery_time' => '12:30:00'],
            ['name' => '13:00', 'delivery_time' => '13:00:00'],
            ['name' => '13:30', 'delivery_time' => '13:30:00'],
            ['name' => '14:00', 'delivery_time' => '14:00:00'],
            ['name' => '14:30', 'delivery_time' => '14:30:00'],
            ['name' => '15:00', 'delivery_time' => '15:00:00'],
            ['name' => '15:30', 'delivery_time' => '15:30:00'],
            ['name' => '16:00', 'delivery_time' => '16:00:00'],
            ['name' => '16:30', 'delivery_time' => '16:30:00'],
            ['name' => '17:00', 'delivery_time' => '17:00:00'],
            ['name' => '17:30', 'delivery_time' => '17:30:00'],
            ['name' => '18:00', 'delivery_time' => '18:00:00'],
            ['name' => '18:30', 'delivery_time' => '18:30:00'],
            ['name' => '19:00', 'delivery_time' => '19:00:00'],
            ['name' => '19:30', 'delivery_time' => '19:30:00'],
            ['name' => '20:00', 'delivery_time' => '20:00:00'],
            ['name' => '20:30', 'delivery_time' => '20:30:00'],
            ['name' => '21:00', 'delivery_time' => '21:00:00'],
            ['name' => '21:30', 'delivery_time' => '21:30:00'],
            ['name' => '22:00', 'delivery_time' => '22:00:00'],
        ]);
    }
}
