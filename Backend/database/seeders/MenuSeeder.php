<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            ['id' => '72ed1b34-8a02-417a-8fad-796cbe31556e', 'name' => 'Домашняя еда', 'restaurant_id' => '59c9233d-65a8-4cf1-83ce-9b2864b61ba6'],
            ['id' => '363f5b2c-8fce-4ab2-9e4e-64551ab98a45', 'name' => 'Итальянская кухня', 'restaurant_id' => 'bf0db591-748a-484f-95b7-2a428d74cc1b'],
            ['id' => 'bce0a3a0-fb1c-4670-a336-5e803b93e8b5', 'name' => 'Азиатская кухня', 'restaurant_id' => '956c9901-dd5b-4411-bb48-e9159fd90f00'],
            ['id' => '3b864c43-ed59-4d7b-aefd-61dd6b879622', 'name' => 'Десерты', 'restaurant_id' => '59c9233d-65a8-4cf1-83ce-9b2864b61ba6'],
            ['id' => '785ed8aa-7b12-4da8-a829-ea9c30d67159', 'name' => 'Десерты', 'restaurant_id' => 'bf0db591-748a-484f-95b7-2a428d74cc1b'],
            ['id' => '515eed03-36c8-442d-be4f-3489c827e5de', 'name' => 'Напитки', 'restaurant_id' => '084ea8b8-9b06-487e-8d13-173860f5a468'],
            ['id' => 'cee16a65-d00b-455a-b74e-777b2b3e5882', 'name' => 'Азиатская кухня', 'restaurant_id' => 'd96c543b-de7c-4692-8e5a-b309e81972de'],
            ['id' => 'f5ceac29-59f9-41a2-b219-fd27ecd4dea8', 'name' => 'Итальянская кухня', 'restaurant_id' => '084ea8b8-9b06-487e-8d13-173860f5a468'],
            ['id' => '5c195a97-8c22-447b-a7e5-5f3e3bf3d4d9', 'name' => 'Азиатская кухня', 'restaurant_id' => '4b0c1d09-1945-4534-8001-2da4d0353dcd'],
            ['id' => '25fca7aa-cbb6-4df4-9004-b2c1e72264df', 'name' => 'Домашняя еда', 'restaurant_id' => '4335d7c3-6d69-4097-beec-150a47dd74e2'],
            ['id' => '92c10520-1bfd-452a-86e7-b9b84602d2da', 'name' => 'Десерты', 'restaurant_id' => '1bfe802b-2f59-440b-b680-cccac31ba3bd'],
            ['id' => '18817fad-a2cd-4039-81dd-35c12f3ebf82', 'name' => 'Азиатская кухня', 'restaurant_id' => 'b15fd43e-c3d3-456f-9ac5-b7a0d9fdf696'],
            ['id' => '0fa82703-940d-4a45-9ef0-d39574a223b2', 'name' => 'Напитки', 'restaurant_id' => '59c9233d-65a8-4cf1-83ce-9b2864b61ba6'],
            ['id' => '8d07e2d7-ee70-4ae1-b580-a7a46ff6f982', 'name' => 'Итальянская кухня', 'restaurant_id' => '4409fe6e-ae9f-4d4f-a246-f6ac49ff8eb7'],
            ['id' => '26bafd9f-1d55-4a9e-9edc-ede1b8ae1fd7', 'name' => 'Десерты', 'restaurant_id' => 'f1a93162-2696-4775-9f70-81e2273fdd07'],
            ['id' => '9fa0081c-f49d-4e4c-a5f3-14067c9ddcbb', 'name' => 'Напитки', 'restaurant_id' => 'bf0db591-748a-484f-95b7-2a428d74cc1b'],
            ['id' => '99fc0eb8-a453-4924-ab99-97b0f74ec512', 'name' => 'Напитки', 'restaurant_id' => '4409fe6e-ae9f-4d4f-a246-f6ac49ff8eb7'],
            ['id' => '83573128-5834-4018-941e-174c1f456ce0', 'name' => 'Домашняя еда', 'restaurant_id' => '0f7be13c-a452-4d5a-95c0-a0c078e91670'],
            ['id' => '7b6c7aca-8675-4f08-93c3-94671639481c', 'name' => 'Азиатская кухня', 'restaurant_id' => 'bbb426ba-8e1e-475c-b9e9-aee84744dded'],
            ['id' => 'f2f5e040-c38b-4919-92a0-303677723836', 'name' => 'Десерты', 'restaurant_id' => '0c50f2c4-7265-4040-992d-0df989d327da'],
            ['id' => 'c6f3b8bd-5f36-4742-a1f7-c8071ac92284', 'name' => 'Итальянская кухня', 'restaurant_id' => '956c9901-dd5b-4411-bb48-e9159fd90f00'],
        ]);
    }
}
