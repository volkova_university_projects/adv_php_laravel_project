<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->integer('cart_number')->autoIncrement();
            $table->string('user_id');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE carts ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('carts_dishes', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->foreignUuid('cart_id')->constrained('carts')->cascadeOnDelete();
            $table->foreignUuid('dish_id')->constrained('dishes')->cascadeOnDelete();
            $table->integer('count');
        });
        DB::statement('ALTER TABLE carts_dishes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
        Schema::dropIfExists('carts_dishes');
    }
};
