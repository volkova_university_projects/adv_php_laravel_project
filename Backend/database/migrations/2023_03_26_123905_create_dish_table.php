<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dishes', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('dish_name');
            $table->foreignUuid('category')->references('id')->on('categories');
            $table->float('price');
            $table->string('description')->nullable();
            $table->boolean('is_vegetarian')->default(false);
            $table->string('photo')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE dishes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dishes');
    }
};
