<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('name');
            $table->foreignUuid('restaurant_id')->references('id')->on('restaurants');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE menus ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('dishes_menus', function (Blueprint $table) {
           $table->uuid('id')->unique();
           $table->foreignUuid('dish_id')->constrained('dishes')->cascadeOnDelete();
           $table->foreignUuid('menu_id')->constrained('menus')->cascadeOnDelete();
        });
        DB::statement('ALTER TABLE dishes_menus ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
        Schema::dropIfExists('dishes_menus');
    }
};
