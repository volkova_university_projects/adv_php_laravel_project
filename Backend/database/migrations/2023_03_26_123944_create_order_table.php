<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->foreignUuid('cart_id')->references('id')->on('carts');
            $table->string('address');
            $table->datetime('order_time');
            $table->dateTime('user_delivery_time');
            $table->foreignid('timeslot')->references('id')->on('timeslots');
            $table->dateTime('system_delivery_time')->nullable();
            $table->dateTime('result_delivery_time')->nullable();
            $table->float('price');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE orders ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
