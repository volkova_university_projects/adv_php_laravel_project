<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('name')->unique();
            $table->string('type');
            $table->string('size');
            $table->string('path');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE photos ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

//        Schema::create('photos_dishes', function (Blueprint $table) {
//            $table->uuid('id')->unique();
//            $table->foreignUuid('dish_id')->constrained('dishes')->cascadeOnDelete();
//            $table->foreignUuid('photo_id')->constrained('photos')->cascadeOnDelete();
//            $table->timestamps();
//        });
//
//        DB::statement('ALTER TABLE photos_dishes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
//        Schema::dropIfExists('photos_dishes');
    }
};
