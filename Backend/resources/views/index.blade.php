<title>All photos</title>
<style>
    h1 {
        font-family: "Arial", serif;
        font-size: large;
    }
    h3 {
        font-family: "Arial", serif;
        font-size: small;
    }
</style>
<h1>All photos from storage</h1>
<div class="row">
    @foreach ($photoUrls as $photoUrl)
        <div class="col-md-4">
            <div class="card mb-4">
                <h3><a href="http://localhost:8000{{$photoUrl}}">{{$photoUrl}}</a></h3>
                <img src="{{ $photoUrl }}" class="card-img-top" alt="..." width="500">
            </div>
        </div>
    @endforeach
</div>

