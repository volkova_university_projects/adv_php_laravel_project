<title>Add file</title>
<style>
    h1 {
        font-family: "Arial", serif;
        font-size: large;
    }
    h3 {
        font-family: "Arial", serif;
        font-size: small;
    }
</style>\
<div class="bg-light p-5 rounded">
    <h1>Add file</h1>
    <div class="form-group mt-4">
        <input type="file" name="file" class="form-control" accept=".jpg,.jpeg,.bmp,.png," title="Choose">
    </div>
    <button class="w-100 btn btn-lg btn-primary mt-4" type="submit">Save file</button>
</div>

{{--@extends('layouts.app-master')--}}

{{--@section('content')--}}
{{--    <div class="bg-light p-5 rounded">--}}
{{--        <h1>Add file</h1>--}}

{{--        <div class="form-group mt-4">--}}
{{--            <input type="file" name="file" class="form-control" accept=".jpg,.jpeg,.bmp,.png,">--}}
{{--        </div>--}}

{{--        <button class="w-100 btn btn-lg btn-primary mt-4" type="submit">Save</button>--}}
{{--        <form action="{{ route('create') }}" method="post" enctype="multipart/form-data">--}}
{{--            @include('layouts.partials.messages')--}}
{{--            @csrf--}}
{{--            <div class="form-group mt-4">--}}
{{--                <input type="file" name="file" class="form-control" accept=".jpg,.jpeg,.bmp,.png,">--}}
{{--            </div>--}}

{{--            <button class="w-100 btn btn-lg btn-primary mt-4" type="submit">Save</button>--}}
{{--        </form>--}}

{{--    </div>--}}
{{--@endsection--}}
