<!DOCTYPE html>
<html>
<head>
    <title>How to Upload Image in Laravel 9? - LaravelTuts.com</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading mt-5 text-center">
            <h2>Upload photo</h2>
        </div>

        <div class="panel-body mt-5">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show mb-2" role="alert">
                    {{ $message }}
                </div>
{{--                @foreach ($photo as $item)--}}
{{--                    <div class="col-md-4">--}}
{{--                        <div class="card mb-4">--}}
{{--                            <img src="{{ $item }}" class="card-img-top" alt="..." width="500">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
                <img src="{{ $photo }}"  alt="">
{{--                <div class="card mb-4">--}}
{{--                    <img src="{{ $photo[0] }}" class="card-img-top" alt="..." width="500">--}}
{{--                </div>--}}
{{--                <img src="images/{{ Session::get('photo') }}" class="mb-2" style="width:400px;height:200px;">--}}
            @endif

            <form action="{{ route('store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="mb-3">
                    <label class="form-label" for="inputImage">Select Image:</label>
                    <input
                        type="file"
                        name="photo"
                        id="inputImage"
                        class="form-control @error('photo') is-invalid @enderror">

                    @error('photo')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="mb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>

            </form>
        </div>
    </div>
</div>
</body>
</html>
