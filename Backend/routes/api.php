<?php

use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\DishController;
use App\Http\Controllers\Api\DishInCartController;
use App\Http\Controllers\Api\MenuController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\RatingController;
use App\Http\Controllers\Api\RestaurantController;
use App\Http\Controllers\Api\TimeslotController;
use App\Http\Controllers\Api\DishInMenuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::controller(CartController::class)->group(function () {
    Route::get('/carts', 'show_all');
    Route::post('/carts', 'create');
    Route::get('/carts/{id}', 'show');
    Route::delete('/carts/{id}', 'delete');
});

Route::controller(DishController::class)->group(function () {
    Route::get('/dishes', 'show_all');
    Route::post('/dishes', 'create');
    Route::get('/dishes/{id}', 'show');
    Route::put('/dishes/{id}', 'update');
    Route::delete('/dishes/{id}', 'delete');
});

Route::controller(DishInCartController::class)->group(function () {
    Route::get('/carts/{id}/dishes', 'show_dishes');
});

Route::controller(DishInMenuController::class)->group(function () {
    Route::get('/menu/{id}/dishes', 'show_dishes');
});

Route::controller(MenuController::class)->group(function () {
    Route::get('/menu', 'show_all');
    Route::post('/menu', 'create');
    Route::get('/menu/{id}', 'show');
    Route::put('/menu/{id}', 'update');
    Route::delete('/menu/{id}', 'delete');
});

Route::controller(OrderController::class)->group(function () {
    Route::get('/orders', 'show_all');
    Route::post('/orders', 'create');
    Route::get('/orders/{id}', 'show');
    Route::put('/orders/{id}', 'update');
    Route::delete('/orders/{id}', 'delete');
});


Route::controller(RestaurantController::class)->group(function() {
    Route::get('/restaurants', 'show_all');
    Route::post('/restaurants', 'create');
    Route::get('/restaurants/{id}', 'show');
    Route::put('/restaurants/{id}', 'update');
    Route::delete('/restaurants/{id}', 'delete');
});

Route::controller(RatingController::class)->group(function() {
    Route::get('/ratings', 'show_all');
    Route::get('/ratings/{id}', 'show');
    Route::put('/ratings/{id}', 'update');
});

Route::controller(TimeslotController::class)->group(function() {
    Route::get('/timeslots', 'show_all');
    Route::post('/timeslots', 'create');
    Route::get('/timeslots/{id}', 'show');
    Route::put('/timeslots/{id}', 'update');
    Route::delete('/timeslots/{id}', 'delete');
});


